# std
from dataclasses import dataclass
from typing import Dict

# contrib
import tensorflow as tf

# local
from gnss_forecasting.managers import Stage, Axis


@dataclass
class DataSliceTF(Switcher):
    """
    N,U,U are fields inherited from Switched
    In this case Dict[str, TensorFlow]
    """
    E_fft: tf.float32
    N_fft: 
    U_fft:   
    pairs_distance: 
    main_position: 
    other_positions: Dict[str, tf.float32]
    encoder: tf.float32

    def x(self)->tf.float32:
        return self.get()

    def y(self)->tf.float32:
        match self.status:
            case Axis.N:
                return self.N_fft
            case Axis.U:
                return self.U_fft
            case Axis.E:
                return self.E_fft

    def distances(self)->tf.float32:
        return self.pairs_distance

    def position(self)->tf.float32:
        return self.main_position, self.other_positions

    def encoder(self)->tf.uint8:
        return self.tensor.to(tf.uint8)

    def to(self, device):
        """
        Move torch elements to device
        """
        [e.to(device) for e in self.E.values()]
        self.E_fft.to(device)
        [n.to(device) for n in self.N.values()]
        #self.N.to(device)
        self.N_fft.to(device)
        [u.to(device) for u in self.U.values()]
        #self.U.to(device)
        self.U_fft.to(device)
        self.pairs_distance.to(device)
        self.main_position.to(device)
        for code, matrix in self.other_positions.items():
            matrix.to(device)
        self.encoder.to(device)
