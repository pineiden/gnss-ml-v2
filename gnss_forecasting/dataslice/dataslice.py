# std
from dataclasses import dataclass, field
from typing import Dict

# contrib
import torch

# local
from gnss_forecasting.managers import Stage, Axis, Switcher


@dataclass
class DataSlice(Switcher):
    """
    N,U,U are fields inherited from Switched
    In this case Dict[str, TensorFlow]
    """
    E_fft: torch.FloatTensor
    N_fft: torch.FloatTensor
    U_fft: torch.FloatTensor  
    pairs_distance: torch.FloatTensor
    main_position: torch.FloatTensor
    other_positions: Dict[str, torch.FloatTensor]
    encoder:torch.FloatTensor
    group_codes:list[str]=field(default_factory=list)

    def x(self)->torch.FloatTensor:
        return self.get()

    def y(self)->torch.FloatTensor:
        match self.status:
            case Axis.N:
                return self.N_fft
            case Axis.U:
                return self.U_fft
            case Axis.E:
                return self.E_fft


    def distances(self)->torch.FloatTensor:
        return self.pairs_distance

    def position(self)->torch.FloatTensor:
        return self.main_position, self.other_positions

    def encoder(self)->torch.ByteTensor:
        return self.tensor.to(torch.uint8)

    def to(self, device):
        """
        Move torch elements to device
        """
        [e.to(device) for e in self.E.values()]
        self.E_fft.to(device)
        [n.to(device) for n in self.N.values()]
        #self.N.to(device)
        self.N_fft.to(device)
        [u.to(device) for u in self.U.values()]
        #self.U.to(device)
        self.U_fft.to(device)
        self.pairs_distance.to(device)
        self.main_position.to(device)
        for code, matrix in self.other_positions.items():
            matrix.to(device)
        self.encoder.to(device)
