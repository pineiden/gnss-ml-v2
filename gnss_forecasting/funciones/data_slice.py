from datetime import datetime, timedelta
from dataclasses import dataclass, asdict

@dataclass
class Interval:
    days:int=0
    hours:int=0
    minutes:int=0
    seconds:int=0


def period(start_date:datetime, interval:Interval):
    return start_date + timedelta(**asdict(interval))
