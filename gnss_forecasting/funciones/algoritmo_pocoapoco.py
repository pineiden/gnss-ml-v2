from rich import print
from typing import Dict, List

def indexes_sets(groups:Dict[str, List[str]]):
    """
    define the sets where every station is used.

    Once used on use item, discart it until all is empty, then
    clear the file loaded.

    """
    indexes = {}

    for name, group in groups.items():
        codes = set([name] + group)
        for code in codes:
            for name,group in groups.items():
                codes_in = set([name] + group)
                if code in codes_in: 
                    if code not in indexes:
                        indexes[code] = {name}
                    else:
                        indexes[code].add(name)
    return indexes


def action(name, group, buffers):
    print(f"Reading...{name} on group {group}")
    buffers[name] = group


def clean(key, name, indexes):
    print(f"Removing {name} on group {key} :: {indexes[key]}")
    indexes[key].discard(name)

if __name__=="__main__":
    grupos = {
        "david":["paula", "pedro"],
        "paula":["alejandra", "elda"],
        "alejandra":["david", "pedro"],
        "elda":["alejandra"],
        "pedro":["david"]
    }
    buffers = {}
    print(grupos)
    indexes = indexes_sets(grupos)
    print(indexes)


    for name, group in grupos.items():
        action(name, group, buffers)
        codes = {name} | set(group)
        for key in codes:
            clean(key, name, indexes)
            if not indexes[key]:
                del buffers[key]

    # if indexes[code] is clean, then delete buffer for this code.

    print(indexes)
    print("buffers", buffers)
