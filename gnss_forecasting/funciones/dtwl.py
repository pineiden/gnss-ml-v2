from tslearn.metrics import SoftDTWLossPyTorch
from dataclasses import dataclass
from gnssml.managers import (
    Axis,
    Switcher)
from torch import nn

@dataclass
class DTW(Switcher):
    """
    Apply the DTW metric to every axis
    """

    @classmethod
    def create(cls):
        N = SoftDTWLossPyTorch(gamma=0.1, normalize=True)
        E = SoftDTWLossPyTorch(gamma=0.1, normalize=True)
        U = SoftDTWLossPyTorch(gamma=0.1, normalize=True)
        return DTW(N=N,E=E,U=U, status=Axis.E)

    def backward(self)->nn.SmoothL1Loss:
        match self.status:
            case Axis.N:
                return self.N.backward()
            case Axis.U:
                return self.U.backward()
            case Axis.E:
                return self.E.backward()
