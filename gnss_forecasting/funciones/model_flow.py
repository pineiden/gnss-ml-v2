import httpx
import time
hostname = "165.227.193.0"

def new_machine(register_params):
    url = f"http://{hostname}:8000/new_machine"
    counter = 0
    while counter<100:
        result = httpx.post(url, json=register_params)
        if result.status_code == 200:
            break
        time.sleep(5)
        counter+=1
    machine = result.json()
    return machine


def get_machine(machine_id):
    url = f"http://{hostname}:8000/machine"
    request = httpx.get(url, params={"machine_id":machine_id})
    assert request.status_code == 200
    result = request.json()
    return result
    

def last_register(machine_id):
    url = f"http://{hostname}:8000/last_register"
    start_stage_request = httpx.get(url, params={"machine_id":machine_id})
    assert start_stage_request.status_code == 200
    result_register = start_stage_request.json()
    return result_register
