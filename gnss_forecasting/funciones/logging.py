import logging
from datetime import datetime
from pathlib import Path

def create_logging(base:Path, model_name:str, level, filename):
    log_path = base.parent / "logs"
    if not log_path.exists():
        log_path.mkdir()
    now = datetime.utcnow()
    now = now.strftime("%Y_%m_%d_%H_%M_%S")
    if not filename:
        filename = log_path / f"{model_name}_{now}.log"
    logging.basicConfig(
        format='%(asctime)s %(levelname)-8s %(message)s',
        filename=filename,
        level=level)
    return filename
