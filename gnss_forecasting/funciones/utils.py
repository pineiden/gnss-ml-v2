from pathlib import Path
from collections import Counter
from datetime import datetime, timedelta
from typing import List, Dict, Any
import pickle
import ujson as json
import numpy as np
import pandas as pd

def read_path(path:Path):
    """
    Leer archivo en bytes dada una ruta
    """
    try:
        databytes = path.read_bytes()
        dataset = pickle.loads(databytes)
        return dataset
    except Exception as e:
        print(e)
        raise e

def count_frequency(codes:List[str]):
    """
    Cuenta la frecuencia de una lista de códigos
    """
    counter = Counter(codes)
    return counter


def circular(array, count, window, future):
    return array[count:count+window+future,:]


def total_seconds_year(ts):
    """
    Segundos del año dado la estampa de tiempo
    """
    ts = int(ts/1000)
    start = datetime.fromtimestamp(ts)
    dt = datetime.fromtimestamp(ts)
    diff = (dt-start).total_seconds()
    return diff


def buffer_sets(groups:Dict[str, List[str]]):
    """
    define the sets where every station is used.

    Once used on use item, discard it until all is empty, then
    clear the file loaded.

    """
    buffers = {}

    #iterate over selection
    for name, group in groups.items():
        codes = set([name] + group)
        for code in codes:
            # iterate again over selection
            for name,group in groups.items():
                codes_in = set([name] + group)
                if code in codes_in: 
                    if code not in buffers:
                        buffers[code] = {name}
                    else:
                        buffers[code].add(name)
    return buffers


def join_fft(df_fft, fft):
    """
    Crea un merge entre dataset y fft al mismo tiempo.
    Sincronizando las series
    """
    try:
        axis_e = fft["E"].values()
        axis_n = fft["N"].values()
        axis_u = fft["U"].values()
        df = pd.DataFrame(columns=["E_fft","N_fft","U_fft"])
        df["E_fft"] = np.concatenate([a for a in axis_e])
        df["N_fft"] = np.concatenate([a for a in axis_n])
        df["U_fft"] = np.concatenate([a for a in axis_u])
        final =  pd.merge(
            df_fft, 
            df, 
            left_index=True,
            right_index=True)
        #breakpoint()
        return final
    except Exception as e:
        raise e

def reverse_stages(dataset_selection):
    """
    Values must be hashable and all different
    crea diccionario reverso.
    """
    reverse = {}
    for stage, values in dataset_selection.items():
        for v in values:
            reverse[v] = stage
    return reverse



def read_json(path:Path):
    databytes = path.read_text()
    dataset = json.loads(databytes)
    return dataset

def get_positions(metadata):
    item = metadata["position"]["llh"]
    return [item["lon"], item["lat"], item["z"]]

def read_encoders(path):
    path = path / "onehot_encodes.data"
    databytes = path.read_bytes()
    return pickle.loads(databytes)

