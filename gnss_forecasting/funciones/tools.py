from pathlib import Path
from datetime import datetime
import pickle
import psutil
import logging
import torch

def dt_filename():
    now = datetime.utcnow()
    filename = now.strftime("%Y_%m_%d_%H_%M_%S")
    return filename

DT_FORMAT ="%Y_%m_%d_%H:%M"

def dt_format(dt:datetime)->str:
    return dt.strftime(DT_FORMAT)

def str_format_dt(dt:str)->datetime:
    return datetime.strptime(dt, DT_FORMAT)


def save_data_loss(modelname,epoch, data, axis, now):
    databytes = pickle.dumps(data)
    loss_dtw_path = Path(__file__).resolve().parent.parent.parent / "loss_dtw"
    logging.info(f"Path loss dtw {loss_dtw_path}")
    loss_dtw_path.mkdir(exist_ok=True)
    filename = loss_dtw_path / f"data_loss_epoch_{epoch}_axis_{axis}_{modelname}_{now}.data"
    filename.write_bytes(databytes)
    return filename

def calculate_dtw(softDTW, y_pred, y_true):
    """
    Calculates the
    Dynamic time warp for timeseries.
    By batch, get mean
    """
    #breakpoint()
    batch_size = len(y_pred)
    y_pred = torch.reshape(y_pred, (batch_size, 1, -1))
    y_true = torch.reshape(y_true, (batch_size, 1, -1))

    dtw_loss = softDTW.get()
    result = dtw_loss(y_true, y_pred).mean() 
    return result

def get_memory():
    virtual = psutil.virtual_memory()
    swap = psutil.swap_memory()
    logging.warning(f"""RAM:: Available {virtual.available}. Used {virtual.percent}% RAM. SWAP {swap.used}/{swap.total} % swap""")
    # gpu memory
    device = torch.device('cuda' if torch.cuda.is_available() else
                          'cpu')
    
    gpumem = torch.cuda.memory_summary(device=device,
                                       abbreviated=False)
    print(gpumem)
    logging.warning(f"GPU memory {gpumem}")

def epoch_time(start_time, end_time):
    elapsed_time = end_time - start_time
    elapsed_mins = int(elapsed_time / 60)
    elapsed_secs = int(elapsed_time - (elapsed_mins * 60))
    return elapsed_mins, elapsed_secs



