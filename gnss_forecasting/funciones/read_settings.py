try:
    import tomllib as toml
except:
    import toml
from pathlib import Path



def read_file(path:Path):
    txt = path.read_text()
    settings = toml.loads(txt)
    return settings
