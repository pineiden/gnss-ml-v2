from torch import nn
import logging
import torch.nn.functional as F
import torch


TIME_FIELDS = [
    "year",
    "week",
    "weekday",
    "ts_daily",
    "timestamp",
]


class LSTMForget(nn.Module):
    def __init__(self, 
                 device,
                 input_size, # number of features
                 hidden_size, # features on hidden state
                 num_layers, 
                 forget_bias:float=1.0):
        super().__init__()
        self.device = device
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.num_layers = num_layers

        bidirectional = True
        self.D = 2 if bidirectional else 1
        self.lstm = nn.LSTM(
            input_size, 
            hidden_size, 
            num_layers,
            bidirectional=bidirectional,  
            batch_first=True)
        
        # Set forget bias for all LSTM layers
        for name, param in self.lstm.named_parameters():
            if 'bias' in name:
                nn.init.constant_(param, forget_bias)
        
    def forward(self, x):
        # comes with autograd variable->differentiable
        h_0 = torch.zeros(
            self.D * self.num_layers, # bidirection x layers
            x.size(0),  # batch size
            self.hidden_size # hidden size
        ).to(self.device)
        
        c_0 = torch.zeros(
            self.D * self.num_layers, 
            x.size(0), 
            self.hidden_size
        ).to(self.device)
        # Forward pass through LSTM layer
        out, _ = self.lstm(x, (h_0, c_0))

        # take the last time step
        return out[:,-1,:]



def normalize(A):
    B = A - 0
    B /= 24*60*60
    return B


def normalize_diff(A):
    # 10cm 
    top = .15
    B = A / top
    return B

def dist_normalize(A, radio=100_000):
    return A/radio


def normalize_position(A):
    """
    A is a matrix position: [lon, lat, height]

    """
    max_value=180
    A[:,0:2] /= max_value
    max_value=9000
    A[:,2] /= max_value

def create_ts_out(x_ts, future):
    dataset = []
    for i in range(x_ts.shape[0]):
        x_ts_i = x_ts[i,:]
        max_ts = torch.max(x_ts_i)
        current_seconds = max_ts % (24*3600)
         # Compute the difference needed to reach the next minute boundary
        diff_to_next_minute = future - (current_seconds % future)

        # Generate 60 consecutive new seconds starting from the maximum timestamp
        new_seconds = torch.arange(1, future+1).to("cuda") + max_ts
        # If new_seconds exceed the end of the day, loop back to the beginning
        new_seconds %= (24 * 3600)       
        #breakpoint()
        ts_out = torch.cat((x_ts_i, new_seconds), dim=0)
        dataset.append(ts_out)

    ts_out = torch.stack(dataset)

    return ts_out


class RNNLSTM(nn.Module):
    """
    Recursive network with closed loop to maintain memory in context
    and learning to forget. (LSTM pro)

    https://cnvrg.io/pytorch-lstm/
    """

    def __init__(self,channels,*args,**kwargs):
        self.distances = []
        self.position = []
        self.other_positions = {}
        dropout = kwargs.get("dropout",.15) 
        self.window = kwargs.get("window", 120)
        if "window" in kwargs:
            del kwargs["window"]
        # if has no future, just regression
        self.future = kwargs.get("future", 0)
        if "future" in kwargs:
            del kwargs["future"]

        self.layers = kwargs.get("lstm_layers",3)

        super().__init__(*args,**kwargs)
        self.device = torch.device('cuda' if torch.cuda.is_available() else
                          'cpu')

        self.channels = channels

        self.model_parameters = {}
        self.model_parameters["order"] = ["conv1", "pool","conv2",
                                          "conv3", "relu", "fc1",
                                          "fc2", "fc3", "fc4"]
        #self.cnn()
        self.lineal()
        self.recursive()
        #
        self.dropout = nn.Dropout(dropout)
        logging.info("CNN lineal baseline created")

    def set_distances(self, distances):
        self.distances = dist_normalize(distances)

    def set_position(self, position):
        self.position = position

    def set_other_positions(self, other_positions):
        self.other_positions = other_positions

    def set_encoder(self, encoder):
        self.encoder = encoder

    def recursive(self):
        # 120
        base_len = self.window #+ self.future
        # 180
        hidden_size = base_len + self.future
        # layers
        layers = self.layers
        
        feat_other_positions = 12
        bits = 8
        nro_features = 14 + feat_other_positions + bits

        params = {
            "input_size": nro_features,
            "hidden_size": hidden_size,
            "num_layers": layers,
        }
        self.lstm = LSTMForget(device=self.device, **params)
        self.lstm_params = params
        self.model_parameters["lstm"] = self.lstm_params


    def main_parameters(self):
        return {
            **self.model_parameters,
        }


    def lineal(self):
        """
        This is a lineal that delivers the output
        """
        # window size
        base_len = self.window #+ self.future
        # 180
        out_size = (base_len + self.future) 
        self.out_size = out_size
        pre_out_size = out_size * 6
        D = 2
        
        self.fc1 = nn.Linear(180, pre_out_size)
        self.model_parameters["fc1"] = (180, pre_out_size)

        self.fc2 = nn.Linear(pre_out_size, out_size)
        self.model_parameters["fc2"] = (pre_out_size, out_size)

        logging.info(f"""Created lineal layers: {self.fc1}""")
        logging.info(f"""Created lineal layers: {self.fc2}""")
        #breakpoint()

    def forward(self, x_in):
        """
        x_in tensor

        ARCHITECTURE:

        - LSTM
        - LINEAL

        output
        """
        tsn = len(TIME_FIELDS)
        #breakpoint()

        x_in = x_in.to(self.device)
        #timestamp vector
        # every row is the timeserie for this slice
        x_ts  = x_in[:,:,:,:tsn]
        # just uses daily secs:
        x_ts = x_ts[:,:,:,3]
        x_ts = x_ts.view(x_ts.size(0), -1)        
        #x_ts = x_ts.view(x_ts.size(0), -1)

        #x_in = normalize(x_in)
        #matrix measurements grouped
        
        # extract all the xs without timestamp
        x = x_in[:,:,:,tsn::]


        positions = []
        # must repeat the same for every sequence position
        mat_distances = self.distances.repeat(x_ts.shape[0],self.window,1).to(self.device).float()
        mat_position = self.position.repeat(x_ts.shape[0],self.window,1).to(self.device).float()
        normalize_position(mat_position)

        positions.append(mat_position)

        # other posiciones....
        for code, position in self.other_positions.items():
            mat_position = position.repeat(x_ts.shape[0],self.window,1).to(self.device).float()
            normalize_position(mat_position)
            positions.append(mat_position)
            

        #breakpoint()
        total_mat_position = torch.cat(positions, axis=2)
        
        ####
        x = normalize_diff(x)

        x  = x.squeeze(1)



        # normalize
        ts_out_n = normalize(x_ts)
        ts_ns = ts_out_n.unsqueeze(2)

        # add another dimension
        #ts_out_n_s = ts_out_n.unsqueeze(1)
        
        # max distance by group: 100km
        mat_distances = dist_normalize(mat_distances)
        #mat_distances = mat_distances.unsqueeze(2)

        # max lat/lon 180
        # concatenate the x (diff distance), ts_out_n (time
        # normalized), mat_distances, mat_position

        #breakpoint()
        encoder_cuda = self.encoder.repeat(x_ts.shape[0],self.window,1).float().to(self.device)

        x = torch.cat(
            [
                x, 
                ts_ns, 
                mat_distances, 
                total_mat_position,
                encoder_cuda
            ], axis=2)
        #breakpoint()

        #breakpoint()
      
        # check shapes : x
        x = self.lstm(x)
        # get only hiddensize
        x = x[:,:,:self.out_size]
        
        # define la mitad
        # base_len output
        x = self.dropout(x)
        x = F.tanh(x)

        x = self.fc1(x)
        x = F.relu(x)

        x = self.fc2(x)
        return x.squeeze(), x_ts


