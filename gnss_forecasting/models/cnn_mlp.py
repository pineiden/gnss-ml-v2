from torch import nn
import logging
import torch.nn.functional as F
import torch


TIME_FIELDS = [
    "year",
    "week",
    "weekday",
    "ts_daily",
    "timestamp",
]

def normalize(A):
    B = A - 0
    B /= 24*60*60
    return B


def normalize_diff(A):
    # 10cm 
    top = .15
    B = A / top
    return B

def dist_normalize(A, radio=100_000):
    return A/radio

def normalize_position(A):
    """
    A is a matrix position: [lon, lat, height]

    """
    max_value=180
    A[:,0:2] /= max_value
    max_value=9000
    A[:,2] /= max_value




class CNNLinealBaseline(nn.Module):
    def __init__(self,channels,*args,**kwargs):
        self.distances = []
        self.position = []
        self.other_positions = {}
        self.encoder = []

        dropout = kwargs.get("dropout",.15) 
        self.window = kwargs.get("window", 120)
        self.future = kwargs.get("future", 0)
        if "window" in kwargs:
            del kwargs["window"]
        if "future" in kwargs:
            del kwargs["future"]
        super().__init__(*args,**kwargs)
        self.device = torch.device('cuda' if torch.cuda.is_available() else
                          'cpu')

        self.channels = channels

        self.model_parameters = {}
        self.model_parameters["order"] = ["conv1", "pool","conv2",
                                          "conv3", "relu", "fc1",
                                          "fc2"]

        self.model_parameters["prediction"] = {"window":self.window, "future":self.future}
        self.cnn()
        self.lineal()
        #
        self.dropout = nn.Dropout(dropout)
        logging.info("CNN lineal baseline created")

    def set_distances(self, distances):
        self.distances = dist_normalize(distances)

    def set_position(self, position):
        self.position = position

    def set_encoder(self, encoder):
        self.encoder = encoder

    def set_other_positions(self, other_positions):
        self.other_positions = other_positions

    def cnn(self):
        out_channels = 8*self.channels
        params = {
            "in_channels": self.channels,
            "out_channels": out_channels,
            "kernel_size": (3,3),
        }
        self.conv1 = nn.Conv2d(**params)
        self.model_parameters["conv1"] = (self.channels, out_channels)
        in_channels = out_channels
        out_channels = 48*self.channels
        params = {
            "in_channels": in_channels,
            "out_channels": out_channels,
            "kernel_size": (1,1),
        }
        self.conv2 = nn.Conv2d(**params)
        self.model_parameters["conv2"] = (in_channels, out_channels)

        in_channels = out_channels
        out_channels = 164*self.channels
        params = {
            "in_channels": in_channels,
            "out_channels": out_channels,
            "kernel_size": (1,1),
        }

        self.conv3 = nn.Conv2d(**params)
        self.model_parameters["conv2"] = (in_channels, out_channels)

        logging.info("Created convolution layers")

    def lineal(self):

        # window size
        base_len = self.window + self.future
        print("BASE LEN", base_len)
        # output: 600 points
        len_group = 5
        len_pos = 15 
        bites = 8
        # the number of points output of conv stage is heavily
        # determinied by windows size.
        # window: 60 -> 4816

        sizes = {
            120:{
                "fc1":9796,
            },
            360:{
                "fc1":29724-(len_group+len_pos) 
            },
            600:{
                "fc1":49644-(len_group+len_pos)
            }
        }
        #breakpoint()
        len_input = sizes[self.window]["fc1"] + len_group + len_pos + bites
        self.fc1 = nn.Linear(len_input, 7200)
        self.model_parameters["fc1"] = (len_input, 7200)

        self.fc2 = nn.Linear(7200, base_len*12)
        self.model_parameters["fc2"] = (7200, base_len*12)

        self.fc3 = nn.Linear(base_len*12, base_len)
        self.model_parameters["fc2"] = (base_len*12, base_len)

        logging.info(f"Created lineal layers: {self.fc1} - {self.fc2} -{self.fc3}")

    def forward(self, x_in):

        #breakpoint()
        tsn = len(TIME_FIELDS)
        # check shape and timestamp column
        
        x_in = x_in.to(self.device)
        #timestamp vector
        x_ts  = x_in[:,:,:,:tsn]
        # extract just ts_daily

        x_ts = x_ts[:,:,:,3]
        # change this
        x_ts = x_ts.view(x_ts.size(0), -1)
        #matriz measurements grouped

        x = x_in[:,:,:,tsn::]
        x = normalize_diff(x)

        start = x
        # x es 600x5 -> batch: [N,1,600,5] 
        # breakpoint()
        x1  = self.conv1(x)
        x = F.relu(F.max_pool2d(x1,2))
        x = self.dropout(x)

        x2  = self.conv2(x)
        x3 = self.conv3(x2)

        x = self.dropout(x3)

        x = x.view(start.size(0), -1)
        ts = x_ts
        x_ts = normalize(x_ts)

        positions = []

        mat_distances = self.distances.repeat(x_ts.shape[0],1).to(self.device).float()
        mat_position = self.position.repeat(x_ts.shape[0],1).to(self.device).float()
        normalize_position(mat_position)

        positions.append(mat_position)

        # other posiciones....
        for code, position in self.other_positions.items():

            mat_position = position.repeat(x_ts.shape[0],1).to(self.device).float()
            normalize_position(mat_position)
            positions.append(mat_position)
            
        total_mat_position = torch.cat(positions, axis=1)

        # here join with 'distances'
        #breakpoint()
        encoder_cuda = self.encoder.repeat(x_ts.shape[0],1).float().to(self.device)

        x = torch.cat([
            x, 
            x_ts, 
            mat_distances, 
            total_mat_position, 
            encoder_cuda], axis=1)
        #breakpoint()
        # check shape before lineal layer

        x = self.fc1(x)

        x = self.dropout(x)

        # relu-relu :: disminuye mucho
        # tanh-relue :: ajust muy pronto a 0
        x = F.tanh(x)

        x = self.fc2(x)

        x = F.relu(x)

        x = self.dropout(x)

        x = self.fc3(x)

        return x.squeeze(), ts


    def main_parameters(self):
        return {
            **self.model_parameters,
        }
