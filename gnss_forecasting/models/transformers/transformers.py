from torch import nn
import logging
import torch.nn.functional as F
import torch
from torch import Tensor
from collections import deque 
import math
from .encoder import PositionalEncoder

TIME_FIELDS = [
    "year",
    "week",
    "weekday",
    "ts_daily",
    "timestamp",
]


def normalize(A):
    B = A - 0
    B /= 24*60*60
    return B

def dist_normalize(A, radio=100_000):
    return A/radio



def normalize_diff(A):
    # 10cm 
    top = .15
    B = A / top
    return B


def normalize_position(A):
    """
    A is a matrix position: [lon, lat, height]

    """
    max_value=180
    A[:,0:2] /= max_value
    max_value=9000
    A[:,2] /= max_value


def create_ts_out(x_ts, future):
    dataset = []
    for i in range(x_ts.shape[0]):
        x_ts_i = x_ts[i,:]
        max_ts = torch.max(x_ts_i)
        current_seconds = max_ts % (24*3600)
         # Compute the difference needed to reach the next minute boundary
        diff_to_next_minute = future - (current_seconds % future)

        # Generate 60 consecutive new seconds starting from the maximum timestamp
        new_seconds = torch.arange(1, future+1).to("cuda") + max_ts
        # If new_seconds exceed the end of the day, loop back to the beginning
        new_seconds %= (24 * 3600)       
        #breakpoint()
        ts_out = torch.cat((x_ts_i, new_seconds), dim=0)
        dataset.append(ts_out)

    ts_out = torch.stack(dataset)

    return ts_out




class Transformer(nn.Module):
    """
    """

    def __init__(self,
                 channels,
                 input_dim: int, 
                 num_heads: int, 
                 num_encoder_layers: int,
                 num_decoder_layers: int, 
                 dim_feedforward: int,
                 dropout: float,
                 *args,
                 **kwargs):
        super().__init__()

        self.distances = []
        self.position = []
        self.input_dim = input_dim
        self.num_heads = num_heads
        self.num_encoder_layers = num_encoder_layers
        self.num_decoder_layers = num_decoder_layers
        self.dropout = dropout
        self.dim_feedforward = dim_feedforward

        self.window = kwargs.get("window", 120)
        self.future = kwargs.get("future", 60)
        self.output_dim = self.window + self.future

        self.device = torch.device('cuda' if torch.cuda.is_available()
                                   else 'cpu')

        self.channels = channels

        self.model_parameters = {}
        self.model_parameters["order"] = ["fc_in", "encoder","transformer",
                                          "fc_out"]
        self.layers()
        #
        logging.info("CNN lineal baseline created")       
        # parameters
        self.base_len = self.window + self.future

    def set_distances(self, distances):
        self.distances = dist_normalize(distances)

    def set_position(self, position):
        self.position = position

    def set_other_positions(self, other_positions):
        self.other_positions = other_positions

    def set_encoder(self, encoder):
        self.encoder = encoder


    def layers(self):
        # input_dim: columns
        # model_dim: columns*3

        self.pos_encoder = PositionalEncoder(
            self.input_dim, 
            max_len=self.output_dim)

        # encoder layer
        encoder_layer = nn.TransformerEncoderLayer(
            d_model=self.input_dim, 
            nhead=self.num_heads,
            dim_feedforward=self.dim_feedforward,
            dropout=self.dropout,
            batch_first=True  # Ensure batch dimension comes first
        )
        self.transformer_encoder = nn.TransformerEncoder(
            encoder_layer, 
            self.num_encoder_layers)

        # decoder layer
        decoder_layer = nn.TransformerDecoderLayer(
            d_model=self.input_dim, 
            nhead=self.num_heads,
            dim_feedforward=self.dim_feedforward,
            dropout=self.dropout,
            batch_first=True  # Ensure batch dimension comes first
        )
        self.transformer_decoder = nn.TransformerDecoder(
            decoder_layer, 
            self.num_decoder_layers)

        self.tgt_transform = nn.Linear(
            1, 
            self.input_dim)  # To
        # transform tgt to the correct dimension

        self.output_linear = nn.Linear(
            self.input_dim, 
            1)

        
        

    def main_parameters(self):
        return {
            **self.model_parameters,
        }



    def forward(
            self, 
            x_in:Tensor, 
            tgt:Tensor, 
            src_mask:Tensor|None=None,
            tgt_mask:Tensor|None=None,
            memory_mask:Tensor|None=None,
            *args):
        """
        x_in tensor

        ts_out: timestamps for output
        """
        tsn = len(TIME_FIELDS)

        tgt = tgt.to(self.device)
        x_in = x_in.to(self.device)
        # extract time matrix
        
        # tiempo

        x_ts  = x_in[:,:,:,:tsn]
        # just uses daily secs:
        x_ts1 = x_ts[:,:,:,3]
        x_ts1 = x_ts1.view(x_ts.size(0), -1)        

        x_ts2 = x_ts[:,:,:,0]
        x_ts2 = x_ts2.view(x_ts.size(0), -1)        

        x_ts3 = x_ts[:,:,:,1]
        x_ts3 = x_ts3.view(x_ts.size(0), -1)        

        # needs to be normalized
        x = x_in[:,:,:,tsn::]


        #total-mat position 
        positions = []
        # must repeat the same for every sequence position
        mat_position = self.position.repeat(
            x_ts.shape[0],self.window,1).to(self.device).float()
        normalize_position(mat_position)

        positions.append(mat_position)

        for code, position in self.other_positions.items():
            mat_position = position.repeat(
                x_ts.shape[0],self.window,1).to(self.device).float()
            normalize_position(mat_position)
            positions.append(mat_position)
        total_mat_position = torch.cat(positions, axis=2)

        # mat distances
        mat_distances = self.distances.repeat(
            x_ts.shape[0],self.window,1).to(self.device).float()
        mat_distances = dist_normalize(mat_distances)
        # encoder values
        encoder_cuda = self.encoder.repeat(
            x_ts.shape[0],self.window,1).float().to(self.device)

        # to normalize
        x = normalize_diff(x)
        x  = x.squeeze(1)

        # ts normalize
        # normalize
        ts_out_n = normalize(x_ts1)
        ts_ns = ts_out_n.unsqueeze(2)

        ts_out_n2 = x_ts2
        ts_ns2 = ts_out_n2.unsqueeze(2)

        ts_out_n3 = x_ts3
        ts_ns3 = ts_out_n3.unsqueeze(2)


        x = torch.cat([
            x, 
            ts_ns, 
            ts_ns2,
            ts_ns3,
            mat_distances, 
            total_mat_position,
            encoder_cuda
        ], axis=2)
        #breakpoint()
        # check shape before lineal layer

        # base_len input and include timestamps for window+future
        # adjust the array

        # here join with 'distances'
        # transformer part
        # permute to: [seqlength, batchsize, feat_dim]
        # shape: Lineal :  [feat_dim, dim_model]
        # pass through encoder

        src = self.pos_encoder(x)
        # go tro transformer-encoder
        # (seq_length, batch_size, d_model)
        memory = self.transformer_encoder(src)

        # Transform tgt to match input_dim
        tgt = tgt.unsqueeze(-1)
        tgt = self.tgt_transform(tgt)  
        tgt = self.pos_encoder(tgt) 

        # output with both encoding
        output = self.transformer_decoder(
            tgt, 
            memory,
            tgt_mask=tgt_mask, 
            memory_mask=memory_mask)
        output = self.output_linear(output)
        output = output.squeeze(-1)  
        return output, ts_ns


