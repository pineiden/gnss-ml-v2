import torch
import torch.nn as nn
import math

class PositionalEncoder(nn.Module):
    def __init__(self, d_model, max_len=5000):
        super().__init__()
        self.d_model = d_model
        self.max_len = max_len
        
        # Learnable positional encodings
        self.positional_encoding = nn.Parameter(torch.randn(max_len, d_model))
        
        # Learnable weight vector
        self.weight = nn.Parameter(torch.ones(max_len))
        
        # Fixed positional encoding using sine, cosine, and exponential functions
        pe = torch.zeros(max_len, d_model)
        position = torch.arange(0, max_len, dtype=torch.float).unsqueeze(1)
        div_term = torch.exp(
            torch.arange(0, d_model,dtype=torch.float) * (-math.log(10000.0)/ d_model)
        )
        
        pe[:, 0::3] = torch.sin(position * div_term[0::3])
        pe[:, 1::3] = torch.cos(position * div_term[1::3])
        pe[:, 2::3] = torch.exp(position / max_len)  # Exponential
        # function
       
        pe = pe.unsqueeze(0)  # Shape: [1, max_len, d_model]
        self.register_buffer('pe', pe)

    def forward(self, x):

        batch_size, seq_len, _ = x.size()
        
        # Apply weights to the fixed positional encoding
        weight = self.weight[:seq_len].unsqueeze(0).unsqueeze(2)
        weighted_pe = self.pe[:, :seq_len, :] * weight
        
        # Combine learnable positional encoding with weighted fixed positional encoding
        combined_pe = self.positional_encoding[:seq_len, :] + weighted_pe.squeeze(0)
        
        # Expand combined positional encoding to match batch size
        combined_pe = combined_pe.unsqueeze(0).expand(batch_size, -1, -1)
        
        return x + combined_pe
