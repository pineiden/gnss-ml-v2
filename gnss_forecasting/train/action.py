from gnss_forecasting.managers import (
    Axis,
    UniversalManager,
    LossAndMetricsManager)
from gnss_forecasting.funciones.tools import (
    dt_filename, 
    save_data_loss, 
    calculate_dtw, 
    epoch_time,
    get_memory)
import logging
import torch
import os
from gnss_forecasting.evaluate import Evaluate

#disable benchmark
torch.backends.cudnn.enabled = False
#torch.cuda.set_enabled_lms(True)
os.environ["PYTORCH_CUDA_ALLOC_CONF"] ="max_split_size_mb:512"
os.environ["COMMANDLINE_ARGS"]="--xformers --no-half --opt-sdp-attention --medvram"

from datetime import datetime, timedelta

TIME_OPTS = {
    "minutes":10,
}

"""
Saving model every 1 hour of training

"""

def train(
        iterator, 
        universal:UniversalManager,
        device:str, 
        epoch:int,
        loss_metrics:LossAndMetricsManager, 
        softDTW, 
        axes, 
        machine, 
        evaluate:Evaluate,
        model_name:str="baseline"):
    #We have to set the neural network in training mode. This is because during
    #training, we need gradients and complementary data to ease the computation  
    cpu = torch.device('cpu')
    next_time = datetime.utcnow() + timedelta(**TIME_OPTS)
    #Training loop
    this_main_code = ""
    flag = True
    #breakpoint()
    for i, ((main_code,group,ts_fft), selector) in enumerate(iterator):
    
        if flag:
            # not run evalue at first time
            this_main_code = main_code
            flag = False

        if this_main_code!=main_code:
            #"on change, then evalate"
            print(f"Running evalue for {this_main_code}")
            evaluate.set_station(main_code)
            evaluate.run(epoch)
            flag = True

        universal.train()
        loss_metrics.new_metrics(epoch)
        if i%100==0:
            get_memory()
            dt = datetime.utcnow()
            print(f"{main_code} :: Allocated GPU Mem - {dt}", torch.cuda.memory_allocated())

        #for axis in Axis:
        for axis in axes:
            loss_metrics.set_status(axis)
            selector.set_status(axis)
            #breakpoint()

            (x, x_err, y), distances, position, other_positions, checkpoint, encoder = selector.values()
            #y = y[1,:].squeeze(dim=2)
            logging.info(f"{main_code}:: Iteraring on axis {axis}")
            universal.set_status(axis)
            softDTW.set_status(axis)
            model, optimizer, criterion = universal.objects()
            model.to(device)
            model.set_distances(distances)
            model.set_position(position)
            model.set_other_positions(other_positions)
            model.set_encoder(encoder)


            optimizer.zero_grad() #Clean gradients
            x = x.cuda()
            x_err = x_err.cuda()
            y = y.cuda()
            if model_name=="Transformer":
                y_pred, ts = model(x,y) #Feed the network with data
            else:
                y_pred, ts = model(x) #Feed the network with data

            logging.info(f"{main_code}:: Y_pred dtype {y_pred.dtype}")
            logging.info(f"{main_code}:: y {y.dtype}")

            loss = criterion(y_pred, y) #Compute the loss
            dtw = calculate_dtw(softDTW, y_pred, y) #Compute the accuracy
            #loss.backward() #Compute gradients

            dtw.backward()

            #model.to(cpu)
            optimizer.step() #Apply update rules

            # fille the loss and metrics
            metric = loss_metrics.new_loss_metrics()
            metric.dtw = dtw.item()
            metric.loss = loss.item()
            model.to(cpu)


        now = datetime.utcnow()

        if now >= next_time:
            #

            for axis in axes:
                # save 
                universal.save(epoch, 0, now, axis, machine)

            # calculate next save
            next_time = now + timedelta(**TIME_OPTS)


    if not flag:
        #"on change, then evalate"
        print(f"Running evalue for {this_main_code}")
        evaluate.set_station(main_code)
        this_main_code = main_code
        evaluate.run(epoch)
