# std
from pathlib import Path
from typing import List, Dict, Tuple, Any, Optional
from dataclasses import dataclass
from datetime import datetime, timedelta
from functools import reduce
import sys
import copy
import pickle
import logging
import random

# contrib
import ujson as json
import numpy as np
import pandas as pd
from rich import print

# local
from gnss_forecasting.dataslice import DataSlice
from gnss_forecasting.managers import Stage, Axis
import gnss_forecasting.funciones.clases as clases
from gnss_forecasting.funciones.utils import (
    read_path,
    count_frequency,
    circular,
    total_seconds_year,
    buffer_sets,
    join_fft,
    reverse_stages
)

TIME_FIELDS = [
    "year",
    "week",
    "weekday",
    "ts_daily",
    "timestamp",
    "dt_gen"
]


class GNSSDataset:
    def __init__(
            self, 
            dataset:Path, 
            dataset_fft:Path,
            groups:Path,
            pairs_distance:Dict[Tuple[str,str], float],
            dataset_selection:Dict[str, List[int]],
            positions:Dict[str,List[int]],
            codes:list[str],
            stage:str,
            window:int,
            future:int,
            group_point:str, # control where start the iteration
            step:int=5,
            selection:List[str]=["first","second","random"],
            test:bool=False, 
            focus_stations:Optional[list[str]]=None,
            encoders=[],
            *args,
            **kwargs):
        super().__init__(*args,**kwargs)
        self.codes = codes
        self.encoders = encoders
        self.focus_stations = focus_stations
        self.dataset = dataset
        self.test = test
        self.window = window
        self.future = future
        self.step = step
        self.dataset_fft = dataset_fft
        self.selection = selection
        self.groups = {
            k:v for k,v in 
            read_path(groups).items() if k in selection}
        self.re_groups = self.rearrange()
        self.set_checkpoint(group_point)
        self.buffers_datasets = {}
        self.pairs_distance = pairs_distance
        self.main_positions = positions
        # post init values 
        self.reverse = reverse_stages(dataset_selection)
        self.dataset_selection = dataset_selection
        self.data_paths = self.paths()
        self.set_stage(stage)
        self.indexes = self.indexes_sets()
        self.next_checkpoint = None
        #breakpoint()
        self.pre_focus_stations = set()
        if not focus_stations:
            self.focus_stations = set(self.indexes.keys())
        else:
            self.focus_stations = set(focus_stations)

       # breakpoint()

    def set_selection(self, stations):
        self.focus_stations |= set(stations)

    def set_station(self, code):
        self.pre_focus_stations = self.focus_stations
        self.focus_stations = {code}

    def clean_focus(self):
        self.focus_stations = self.pre_focus_stations

    def set_checkpoint(self, code):
        self.checkpoint = None
        if code in self.re_groups:
            self.checkpoint = code

    def set_stage(self, name):
        name = name.lower()
        match name:
            case "train":
                self.stage = Stage.TRAIN
            case "validation":
                self.stage = Stage.VALIDATION
            case "test":
                self.stage = Stage.TEST
            case _:
                self.stage = Stage.TRAIN
                

    def read_input(self, code):
        filepath = self.dataset / code
        return read_path(filepath)

    def read_output(self, code):
        filepath = self.dataset_fft / f"{code}.data"
        logging.info(f"Reading {filepath}")
        return read_path(filepath)


    def indexes_sets(self):
        """
        define the sets where every station is used.
        
        Once used on use item, discart it until all is empty, then
        clear the file loaded.


        Create a counter +1 every time a code is used

        self.groups = {
        mode1:{codes},
        mode2:{codes},
        mode3:{codes}
        }
        """
        order = []
        indexes = {}
        codes = set()
        
        # rescue the codes uses on every mode that must be used on the
        # process, using groups
        total = []
        for name, groups in self.groups.items():
            total += reduce(lambda a,b:a+b, [[k]+v for k,v in groups.items()])
            # for code, codes in groups.items():
            #     if "UDAT" in codes:
            #         print(code,"->", codes)

        indexes = count_frequency(total)

        #breakpoint()
        # keys = self.groups.keys()
        # # make the join
        # last_indexes = {}
        # for code in codes:
        #     last_indexes[code] = set()
        #     for key in keys:
        #         last_indexes[code] |= indexes[key][code]
        # {code:last_indexes[code] for code in order}
        return indexes

    def paths(self):
        data_paths = {s:{}  for s in Stage}

        for path in self.dataset.rglob("*.data"):
            code = path.parent.name
            filename_nro = int(path.name.replace(".data",""))
            #breakpoint()
            stage = self.reverse.get(filename_nro, Stage.TRAIN)
            if code not in data_paths[stage]:
                data_paths[stage][code] = []
            filename = path.name
            data_paths[stage][code].append((filename_nro, path))

        # sorting to take ordering data
        for s, groups in data_paths.items():
            for values in groups.values():
                values.sort(key=lambda x:x[0])

        return data_paths


    def rearrange(self):
        code = self.selection[0]
        codes = list(self.groups[code].keys()) 
        groups = {}
        # every item on selection must be in groups
        for code in codes:
            item = {s:self.groups[s][code] for s in self.selection if
                    s in self.groups}
            groups[code] = item
        # this order allow a more efficient use of the datasets
        return groups

    def build_data_matrix(self, maincode, dataset, fft, cols_map, group_codes):
        """
        dataset : dict ordered with codes and datasets by code

        fft: must be a numpy array, fft has all data output

        On the merge process cuts to intersection
        """

        ts = "timestamp"
        ts_daily = "ts_daily"
        time_fields = [t for t in TIME_FIELDS if t not in {"dt_gen"}]
        df_fft = pd.DataFrame(columns=[ts])
        try:
            dataframes = {}
            for code, values in dataset.items():
                station = pd.DataFrame([data for data in values["timeserie"]])
                dataframes[code] = station

            axes = ["E", "N", "U"]
            dataframes_axis = {}
            df_fft[ts] = fft[ts].apply(lambda x:int(x/1000))
            df_fft = join_fft(df_fft, fft)       
            df_fft.reset_index(inplace=True, drop=True)

        except Exception as e:
            print("Exception create matrix", maincode, e)
            raise e

        for axis in axes:
            colnames = [f"{axis}_{c}" for c in cols_map[axis]]
            df = dataframes[maincode][[*time_fields, *colnames]]
            
            # parse all time fields to int
            for tsfield in time_fields:
                if tsfield=="timestamp":
                    df.loc[:,tsfield] = df[tsfield].apply(lambda x:int(x/1000))
                else:
                    df.loc[:,tsfield] = df[tsfield].apply(int)


            this_colnames = {col:f"{col}_{maincode}" for col
                            in colnames}

            df = df.rename(columns=this_colnames)

            df = pd.merge(
                df_fft[[ts, f"{axis}_fft"]], 
                df, 
                on="timestamp", 
                how="inner")
            group_colnames = list(this_colnames.values())
            for code in dataset.keys():
                if code != maincode:
                    # 
                    dfcode = dataframes[code][[*time_fields,
                                               *colnames]]
                    this_colnames = {col:f"{col}_{code}" for col
                                    in colnames}
                    group_colnames.extend(this_colnames.values())

                    dfcode = dfcode.rename(columns=this_colnames)
                    for tsfield in time_fields:
                        if tsfield=="timestamp":
                            dfcode.loc[:,tsfield] = dfcode[tsfield].apply(lambda x:int(x/1000))
                        else:
                            dfcode.loc[:,tsfield] = dfcode[tsfield].apply(int)
                    df = pd.merge(df,dfcode, on=time_fields,
                                  how="left")
            # all nan, None -> 0
            df = df.fillna(0)
            # sort only by timestamp
            df.sort_values(by=[ts], inplace=True)
            
            group_ordered = []
            for code in group_codes:
                code_cols = [c for c in group_colnames if
                             c.endswith(code)]
                group_ordered.extend(code_cols)
            #breakpoint()
            columns = [*time_fields, f"{axis}_fft"]+group_ordered
            dataframes_axis[axis] = df.reindex(columns=columns)    
        
        #breakpoint()
        # out size must be dataset on timeserie size (low)
        # cleaned 1 may 2024: (items, 21 cols)
        return dataframes_axis

    def matrix_to_numpy(self, matrix, field, cols_map):
        """
        Convert pandas dataframe to torch tensors
        """
        time_fields = [t for t in TIME_FIELDS if t not in {"dt_gen"}]


        fft = f"{field}_fft"
        # the the array for fft signal
        MM_fft = matrix[field][[fft]].to_numpy(dtype=np.float32)
        # get the time fields with fft
        TS_FFT = matrix[field][
            [*time_fields, fft]].to_numpy(dtype=np.float32)

        # this df is just the values without time fields and fft
        # just the matrix with values 'pure_backshifted'
        df = matrix[field].drop(labels=[fft], axis=1)
        #df = matrix[field].drop(labels=[fft], axis=1)
        #breakpoint()
        np_matrix = df.to_numpy(dtype=np.float32)
        # to check
        fields = {c:f"{field}_{c}" for c in cols_map[field]}

        #breakpoint()
        
        DATA_MATRIX = {}
        for vfield in fields.values():
            columns = time_fields + [c for c in df.columns if c.startswith(vfield)]
            selected_matrix = df[columns]
            this_np_matrix = selected_matrix.to_numpy(dtype=np.float32)
            DATA_MATRIX[vfield] = this_np_matrix
        # must return the data:  err, pure_trend, pure_backshifted ,
        # and fft dataset

        #breakpoint()
        return DATA_MATRIX, MM_fft, TS_FFT

    def __iter__(self)->DataSlice:
        # re arrange groups to be more efficient
        groups = self.re_groups
        # a copy in case of reset, the elements will be deleted
        dataset_buffers = {}
        groups_buffers = {k:v for k, v in self.indexes.items()}
        #copy_groups_buffers = copy.deepcopy(groups_buffers)
        #deleted_group = {c:set() for c in self.indexes.keys()}

        print(groups_buffers)

        #breakpoint()
        flag = False
        order = {i:k for k, i in enumerate(self.re_groups.keys())}

        # name is the group of dataset: first, second, random, etc
        if not self.checkpoint:
            flag = True

        for main_code, group in groups.items():
            
            # control if main_code is in the required stations and if
            # exists the dataset for the station
            if main_code in self.focus_stations and main_code in self.codes:
                
                # encoder is the array with 1 and 0 that represents
                # Station's CODE
                encoder = self.encoders.get(main_code)

                # mmmm
                if self.checkpoint!=None and main_code == self.checkpoint:
                    flag = True

                self.next_checkpoint = main_code

                if not flag:
                    # set as empty group
                    # this avoid an if indentation
                    group = {}

                # group = dict { name1: neigh1, name2: neigh2,... }
                # code of the stations an its group
                for name, neighboors in group.items():
                    # rescue the group of stations where the code exists
                    # and could be used
                    n = int(len(neighboors)  / 2)
                    #this order is to manage the convolution with center
                    #on 'main_code' station
                    group_codes = neighboors[0:n] + [main_code] + neighboors[n::]

                    pairs_distance =  np.array([self.pairs_distance[(main_code,code)]
                                  for code in group_codes])

                    main_position =  np.array(self.main_positions.get(main_code))

                    other_positions = {
                        code:
                            np.array(
                                self.main_positions.get(code)
                            )
                        for code in neighboors
                    }

                    # read every station of this group and save on the
                    # buffer:
                    # 1) read the base data timeserie
                    # 2) read the fft out dataset
                    group_dataset = {}
                    # read from files or load from group_dataset
                    if self.test:
                        print("Test workin on", group_codes, datetime.now())
                    for code in group_codes:
                        # generate structure {"fft": Dataset,
                        # "timeserie":List[Path]}
                        if code not in dataset_buffers:
                            start = datetime.utcnow()
                            logging.info(f"READING START:: Reading datasets and fft for {code} - {start}")
                            dataset = {"timeserie":[]}
                            dataset["fft"] = self.read_output(code) if self.test else f"[test] fft for {code}"
                            # get from buffer
                            # data_paths = self.data_paths[code]
                            dataset["timeserie"] = np.concatenate([read_path(path) for
                                                    index,path in
                                                    self.data_paths[self.stage][code]])

                            #breakpoint()
                            # new big dataset to manage the paths inside the group
                            group_dataset[code] = dataset
                            # set the group when the code is inside
                            # make a copy
                            dataset_buffers[code] = dataset
                            end = datetime.utcnow()
                            total = (end-start).total_seconds()
                            logging.info(f"READING END:: Reading datasets and fft for {code} - {end}, {total} secs")

                        else:
                            # main_code or code?
                            # if was readed...
                            group_dataset[code] = dataset_buffers[code]
                    # iterate over group_dataset
                    # and read the files

                    item = group_dataset[main_code]
                    fft = item["fft"]
                    fields = {"pure_backshifted", "pure_trend", "err"}

                    cols_map = {
                        "N":fields,
                        "E":fields,
                        "U":fields,
                    }

                    time_selection = {"ts_daily", "year", "week", "weekday"}

                    # matrix and fft
                    # contains the data for the groups of stations
                    data_matrix = self.build_data_matrix(
                        main_code, 
                        group_dataset, 
                        fft, 
                        cols_map, 
                        group_codes)
                    # matrix by axis to torch

                    # MM_X is a dict with different types fields ::  {"pure_backshifted", "pure_trend", "err"}
                    MM_E,MM_fft_E,TS_FFT_E = self.matrix_to_numpy(
                        data_matrix, "E", cols_map)
                    MM_N,MM_fft_N,TS_FFT_N = self.matrix_to_numpy(
                        data_matrix, "N", cols_map)
                    MM_U,MM_fft_U,TS_FFT_U = self.matrix_to_numpy(
                        data_matrix, "U", cols_map)

                    # to add error, here
                    #breakpoint()
                    window = 0
                    # to use for 
                    len_data = MM_E["E_pure_trend"].shape[0]# len(MM_E)
                    if self.window > len_data or self.window <=1:
                        window = len_data
                    else:
                        window = self.window
                    count = 0


                    while count <= len_data -  window - self.future:
                        win_data = {
                            "E":extract(MM_E, count, window), #[count:count+window,:],
                            "E_fft":MM_fft_E[count:count+window+self.future,:],
                            "N":extract(MM_N,count, window),#[count:count+window,:],
                            "N_fft":MM_fft_N[count:count+window+self.future,:],
                            "U":extract(MM_U, count, window), #[count:count+window,:],
                            "U_fft":MM_fft_U[count:count+window+self.future,:],
                            "pairs_distance": pairs_distance,
                            "main_position": main_position,
                            "other_positions": other_positions,
                            "encoder":encoder
                        }
                        # compose array
                        data = DataSlice(**win_data, status=Axis.E)

                        ts_fft = {
                            "E":TS_FFT_E[count:count+window+self.future,:],
                            "N":TS_FFT_N[count:count+window+self.future,:],
                            "U":TS_FFT_U[count:count+window+self.future,:]
                        }

                        #breakpoint()
                        # compose array end
                        yield (main_code, group_codes, ts_fft), data
                        count += self.step
                        ############ must check
                    # step where the dataset_buffer is cleared if all the
                    # uses for the code is complete
                    # for every code on the neightboour group, get from
                    # buffers and remove main_code from the list
                    # case :: on empty list -> delete from buffers
                    # CHECK AND TEST THIS:
                    # iterating over all codes where MAIN_CODE is used
                    for code in group_codes:
                        # drop one for group_buffers
                        logging.warning(f"FREE:: A:: Free {code} on {group_codes}, count: {groups_buffers[code]}")

                        groups_buffers[code] -= 1
                        # then check the counter                   
                        # if empty go inside this if
                        if groups_buffers[code]==0 and code in dataset_buffers:
                            # all task where main_code is used happened
                            # drop from datasets the data associated to code
                            logging.warning(f"FREE:: F:: Droping all group code {code}")
                            #breakpoint()
                            print(f"FREE:: G:: Deleting on code {code}, counter==0 in dataset_buffers", main_code in dataset_buffers)
                            del dataset_buffers[code]
                            del groups_buffers[code]



def extract(MDICT, count, window):
    return {k:v[count:count+window,:] for k,v in MDICT.items()}


if __name__=="__main__":
    base = Path(__file__).parent.resolve()
    dataset = base.parent / "datasets/timeserie"
    dataset_fft = base.parent / "datasets/fft_out"
    groups = base.parent / "datasets/graph_neightboor_stations_set.data"
    dataset = GNSSDataset(dataset, dataset_fft, groups, test=True)
    print(dataset.groups)
    print(dataset.indexes)
    print(dataset.re_groups)
    #dataloader.paths()
    #print(dataloader.data_paths)
    STOP = 10
    for i, data in enumerate(dataset):
        # data is a DatSlice object
        print(f"Iteracion {i}",data.x().shape, data.y().shape)
        data.set_status("n")
        print(f"Iteracion {i}",data.x().shape, data.y().shape)
        data.set_status("u")
        print(f"Iteracion {i}",data.x().shape, data.y().shape)

        if i == STOP:
            break



