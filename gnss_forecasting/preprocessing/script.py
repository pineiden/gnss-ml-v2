import pickle
from pathlib import Path
from read_data import read_json
from typing import List, Dict
from clases import Pair, Position, TimeInfo, DataItem, GNSSDataset
from datetime import datetime, timedelta
import time
from pydantic.dataclasses import dataclass
from rich import print
import pandas as pd
import json

def get_min_max_dt(path: Path):
    if path.is_dir():
        primeros = []
        ultimos = []
        last_index = {}
        megadataset = {}
        for i, file_path in enumerate(path.rglob("*.json")):
            name = file_path.stem
            try:
                if not name == "metadata" and len(name)==4:
                    print("get min max ... i:>", i)
                    data = read_json(file_path)
                    data.sort(key=lambda x: x.timestamp)
                    first = datetime.fromisoformat(data[0].dt_gen)
                    last = datetime.fromisoformat(data[-1].dt_gen)
                    primeros.append(first)
                    ultimos.append(last)
                    last_index[name] = 0
                    megadataset[name] = []
                    del data
            except Exception as ex:
                print("Error", path, ex)
        return (min(primeros), max(ultimos), last_index, megadataset)
    return []


# @dataclass
# class Slice:
#     start: int
#     end: int

#     def slice(self):
#         return slice(self.start, self.end)

def backshift(df, fields=["N","E","U"]):
    """
    Operation = (1-B)^2 y =  y_t - 2y_{t-1}+y_{t-2}
    """
    instrument_frec = 6
    for column in fields:       
        df[f"{column}_backshifted"] = df[column] - 2 *df[column].shift(1) + df[column].shift(2)
        df[f"{column}_trend"] = + 2 *df[column].shift(1) - df[column].shift(2)
        column_pure = f"{column}_pure"
        df[column_pure] = df[column]  - df[column].shift(6) 
        df[f"{column_pure}_backshifted"] = df[column_pure] - 2 *df[column_pure].shift(1) + df[column_pure].shift(2)
        df[f"{column_pure}_trend"] = + 2 *df[column_pure].shift(1) - df[column_pure].shift(2)
    
from json import JSONEncoder
# subclass JSONEncoder
class DateTimeEncoder(JSONEncoder):
        #Override the default method
        def default(self, obj):
            if isinstance(obj, (datetime.date, datetime.datetime)):
                return obj.isoformat()

def save_to_bytes(dataset, name, counter, save_json):
    dbyts = pickle.dumps(dataset)
    base_path = Path(f"./datasets_post/{name}")
    base_path.mkdir(parents=True, exist_ok=True)
    if save_json:
        txt = json.dumps(dataset, cls=DateTimeEncoder)
        path = base_path / f"{counter}.json"
        path.write_text(txt)
    else:
        path = base_path / f"{counter}.data"
        path.write_bytes(dbyts)
        print(f"Writed file with data {path}")


def read_json_section(
        path,
        start_date,
        end_date,
        last_index,
        megadataset,
        stations,
        hours_lapso=12,
        save_json:bool=False):
    if path.is_dir():
        a = start_date
        paths = sorted([p for p in path.rglob("*.json")], key=lambda
                       x: x.stem)
        print("PATHS===")
        print(paths)

        for i, file_path in enumerate(paths):
            print("Reading file ->", file_path)
            print("Reading file STEM ->", file_path.stem)
            start_date = a
            name = file_path.stem
            try:
                counter = 0
                if len(name) == 4:
                    print("read json section ... i:>", i)
                    dataset = read_json(file_path)
                    ds_dict = [r.usar for r in dataset]
                    print("Len datasset", len(ds_dict))

                    if ds_dict:
                        ds_start_date = ds_dict[0]["dt_gen"]

                        data = pd.DataFrame(ds_dict)
                        data.sort_values(by=["timestamp"], inplace=True)
                        data.reset_index(drop=True)
                        backshift(data)
                        #breakpoint()
                        counter = 0
                        last_date = start_date + timedelta(hours=hours_lapso)

                        if ds_start_date>last_date:
                            start_date = ds_start_date
                            last_date = start_date + timedelta(hours=hours_lapso)

                        print(f"Splitting dataset {name}", data.shape)
                        shape = data.shape[0]

                        while shape>0:
                            dt_slice = data[data.dt_gen.between(start_date, last_date)]

                            # if dataset doesn't have data on...
                            if dt_slice.shape[0]==0:
                                break

                            dt_records = dt_slice.to_dict("records")
                            save_to_bytes(dt_records, name, counter, save_json)

                            print(f"{name} :: Dt slice shape", dt_slice.shape)
                            dt_indexes = dt_slice.index
                            tramo = slice(min(dt_indexes), max(dt_indexes))
                            megadataset[name].append(tramo)
                            data.drop(dt_slice.index, inplace=True)
                            shape = data.shape[0]
                            if shape==0:
                                break
                            counter += 1
                            print(f"{name} ::Dataframe shape::",
                                  data.shape)

                            start_date = data.iloc[0].dt_gen
                            print("new start date", start_date)
                            last_date = start_date + timedelta(hours=hours_lapso)

                            # save dataset
            except Exception as ex:
                print("Error", path, ex)
                raise ex
        return megadataset
    return []


if __name__ == "__main__":
    hours_lapso = 12
    base_path = Path("../../DATA_BACKUP")
    path_base = Path("base_info.data")

    if not path_base.exists():
        start_date, last_date, last_index, megadataset = get_min_max_dt(
            base_path)

        databytes = pickle.dumps(
            (start_date, last_date, last_index, megadataset))
        path_base.write_bytes(databytes)

    else:
        databytes = path_base.read_bytes()
        start_date, last_date, last_index, megadataset = pickle.loads(
            databytes)

    print("dates", start_date, last_date)

    if (b := last_date - timedelta(days=7)) > start_date:
        start_date = b

    print("dates fix", start_date, last_date)

    read_json_section(
        base_path, 
        start_date, 
        last_date,
        last_index, 
        megadataset, 
        hours_lapso, 
        save_json=True)

    print(megadataset)
    databytes = pickle.dumps(megadataset)
    path_sections = Path("data_sections.data")
    path_sections.write_bytes(databytes)
