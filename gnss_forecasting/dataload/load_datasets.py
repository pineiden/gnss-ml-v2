from typing import Dict, List, Any, Optional
from pathlib import Path
from gnss_forecasting.datasets import GNSSDataset
from gnss_forecasting.dataload import GNSSDataloader
from gnss_forecasting.managers import UniversalIterator
import pickle
import json
#Function to test neural network
from gnss_forecasting.funciones.utils import (
    read_path,
    read_json,
    total_seconds_year,
    reverse_stages,
    read_encoders,
    get_positions
)


def load_dataset_iterators(
        path:Path, 
        dataset_selection:Dict[str,List[int]],
        batch_size:int, 
        window:int, 
        future:int,
        group_point:Optional[str]=None,
        field_components:tuple[str,str] = (),
        focus_stations:Optional[list[str]]=[]
):
    encoders = read_encoders(path)
    dataset = path /"timeserie"
    dataset_fft = path / "fft_out"
    groups = path / "graph_neightboor_stations_set.data"
    
    #breakpoint()
    # read this and share in all GNNSdataset
    pairs_distance = read_path(path / "pairs_distance.data")
    positions = {item["code"]:get_positions(item) for item in  read_json(path/"metadata.json")}
    

    codes = []
    for p in dataset.rglob(""):
        if len(p.name) == 4:
            codes.append(p.name)

    train_dataset = GNSSDataset(
        dataset, 
        dataset_fft, 
        groups,
        pairs_distance,
        dataset_selection,
        positions,
        codes,
        stage="train",
        window=window,
        future=future,
        group_point=group_point,
        test=True, 
        focus_stations=focus_stations, 
        encoders=encoders)
    val_dataset = GNSSDataset(
        dataset, 
        dataset_fft, 
        groups,
        pairs_distance,
        dataset_selection,
        positions,
        codes,
        stage="validation",
        window=window,
        future=future,
        group_point=group_point,
        test=True,
        focus_stations=focus_stations, 
        encoders=encoders)
    test_dataset = GNSSDataset(
        dataset, 
        dataset_fft, 
        groups,
        pairs_distance,
        dataset_selection,
        positions,
        codes,
        stage="test",
        window=window,
        future=future,
        group_point=group_point,
        test=True,
        focus_stations=focus_stations,
        encoders=encoders)

    train_dataloader = GNSSDataloader(
        train_dataset, 
        field_components=field_components,
        batch_size=batch_size)
    val_dataloader = GNSSDataloader(
        val_dataset, 
        field_components=field_components,
        batch_size=batch_size)
    test_dataloader = GNSSDataloader(
        test_dataset, 
        field_components=field_components,
        batch_size=batch_size)

    return dataset, UniversalIterator(train_dataloader, val_dataloader, test_dataloader)
