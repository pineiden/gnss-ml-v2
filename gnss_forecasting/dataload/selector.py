from dataclasses import dataclass, field
from typing import Generic, Tuple, TypeVar, Dict
import torch

from gnss_forecasting.datasets import GNSSDataset
from gnss_forecasting.managers import Iterator, Switcher, Axis

TFloat = TypeVar("TorchFloat")

@dataclass
class Selector(Switcher):
    """Selector

    is a Switched with more special attributes
    """
    distances:Dict[Tuple[str,str], float]
    position:torch.FloatTensor
    checkpoint:str
    other_positions: Dict[str, torch.FloatTensor]
    encoder:torch.ByteTensor
    group_codes:list[str] = field(default_factory=list)

    def values(self):
        return (self.get(), self.distances, self.position,
                self.other_positions, self.checkpoint, self.encoder)

