from dataclasses import dataclass
from typing import Generic, Tuple,TypeVar, Dict
import torch

from gnss_forecasting.datasets import GNSSDataset
from gnss_forecasting.managers import Iterator, Switcher, Axis
from .selector import Selector

TFloat = TypeVar("TorchFloat")

@dataclass
class GNSSDataloader(Generic[Iterator]):
    """
    This dataloader doesn't need to know the length of the dataset
    """
    dataset: GNSSDataset
    batch_size: int
    run:bool=True
    device: str = 'cuda' if torch.cuda.is_available() else 'cpu'  
    station:str = ""
    mode:str="train"
    field_components:tuple[str,str] = ()
    # Establecer dispositivo (GPU si está disponible, de lo contrario,
    # CPU)

    @property
    def main_component(self):
        return self.field_components[0]

    @property
    def error_component(self):
        return self.field_components[1]

    def set_station(self, code:str):
        self.station = code

    def set_mode(self, mode:str):
        self.mode = mode

    def __iter__(self):
        AXES = ["N","E","U"]
        X = []
        Y = []
        counter = 0
        count = False
        # declare generic items for x, x_err, y
        item = {axis:{"x":[], "x_err":[],"y":[], "distances":[]} for axis in AXES}
        """
        iterate
        over the generator-iterator
        data is a DataSlice and can deliver every vector given the axis

        """
        if self.mode=="eval":
            print(f"Station on dataloader <{self.station}>")
            self.dataset.set_station(self.station)

        for main_code, data in self.dataset:
            group_codes = data.group_codes
            distances = data.distances()
            position, other_positions = data.position()
            encoder = data.encoder
            datasets = {}
            for axis in AXES:
                data.set_status(axis)
                x = data.x()
                y = data.y()
                # x is the dict
                    
                x_main = x[f"{axis}_{self.main_component}"]
                x_err = x[f"{axis}_{self.error_component}"]
                # default value 600, hardcoded, check how rescue and
                # output must be length window + future prediction
                if (len(x_main)==self.dataset.window and 
                    len(y)==(self.dataset.window + self.dataset.future) ):

                    item[axis]["x"].append(x_main)
                    item[axis]["x_err"].append(x_err)
                    item[axis]["y"].append(y)
                    count = True

            if count:
                counter += 1
                count = False
            if counter==self.batch_size:
                """
                Build the batch tensor with the output

                """
                try:
                    X = item["E"]["x"]
                    X_err = item["E"]["x_err"]
                    Y = item["E"]["y"]
                    batch_x = torch.stack(X).unsqueeze(1)
                    batch_x_err = torch.stack(X_err).unsqueeze(1)
                    batch_y = torch.stack(Y).unsqueeze(1)
                    # this must be returned with selector.get() on train 
                    datasets["E"] = (batch_x, batch_x_err, batch_y.squeeze())
                    X = item["N"]["x"]
                    X_err = item["N"]["x_err"]
                    Y = item["N"]["y"]
                    batch_x = torch.stack(X).unsqueeze(1)
                    batch_x_err = torch.stack(X_err).unsqueeze(1)
                    batch_y = torch.stack(Y).unsqueeze(1)
                    # this must be returned with selector.get() on train 
                    datasets["N"] = (batch_x, batch_x_err, batch_y.squeeze())
                    X = item["U"]["x"]
                    X_err = item["U"]["x_err"]
                    Y = item["U"]["y"]
                    batch_x = torch.stack(X).unsqueeze(1)
                    batch_x_err = torch.stack(X_err).unsqueeze(1)
                    batch_y = torch.stack(Y).unsqueeze(1)
                    # this must be returned with selector.get() on train 
                    datasets["U"] = (batch_x, batch_x_err,batch_y.squeeze())
                    item = {axis:{"x":[], "x_err":[], "y":[]} for axis in AXES}
                    counter = 0
                    yield main_code, Selector(**datasets, 
                                   distances=distances, 
                                   position=position, 
                                   other_positions=other_positions,
                                   status=Axis.E,
                                   checkpoint=self.dataset.next_checkpoint,
                                   encoder=encoder, 
                                   group_codes=group_codes)
                except Exception as ex:
                    raise ex

    def set_status(self,axis:str):
        self.dataset.set_status(axis)

    def stop(self):
        self.run = not self.run
        

