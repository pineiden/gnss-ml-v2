from pathlib import Path
from rich import print
import ujson as json

import typer
from cnn_mlp import run as run_training
from gnss_forecasting.managers import (
    Stage
)
from gnss_forecasting.cobertura.cnn_mlp import run_cobertura

this_directory = Path(__file__).parent.resolve()

def get_positions(metadata):
    item = metadata["position"]["llh"]
    return [item["lon"], item["lat"], item["z"]]

def read_json(path:Path):
    databytes = path.read_text()
    dataset = json.loads(databytes)
    return dataset

def getcode(name):
  code = name.split("_")[-1]
  return code

if __name__=="__main__":
    path = this_directory.parent.parent / "datasets"

    dataset_list = path / "timeserie"
    names = {p.name for p in dataset_list.glob("*")}
    path_loss_dtw = path / "loss_dtw"
    code_done = {getcode(p.stem) for p in path_loss_dtw.glob("*.json")}

    positions = {
        item["code"]:item 
        for item in  read_json(path/"metadata.json") if item["code"]
        in names}

    #breakpoint()
    print(positions)
    arch  = "baseline_cnn"
    for code, item in positions.items():
        if code not in code_done:
            model_path = run_training(code)

            print(f"Model path is {model_path}")
            print("Show models saved")
            for path in model_path.glob("*.pt"):
                print(path)

            run_cobertura(arch,code, model_path, item)
