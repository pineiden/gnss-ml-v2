import logging
import math
import time
from pathlib import Path
from datetime import datetime
from dataclasses import dataclass
from typing import Dict, List, Any, Optional
import numpy as np
from rich import print

import torch
import torch.optim as optim
from torch.autograd import Variable
from torch.nn import functional as F
from torch import nn
from torch.utils.data import DataLoader
from gnss_forecasting.datasets import GNSSDataset
from gnss_forecasting.dataload import GNSSDataloader
from gnss_forecasting.models import RNNLSTM
from gnss_forecasting.managers import (
    Stage,
    Axis,
    Switcher,
    ModelManager,
    OptimizerManager,
    CriterionManager,
    UniversalManager,
    LossAndMetrics,
    LossAndMetricsManager,
    UniversalIterator)
from gnss_forecasting.funciones import create_logging
from gnss_forecasting.funciones import getFolderSize
from gnss_forecasting.funciones import new_machine, last_register, get_machine
from gnss_forecasting.funciones import DTW
from gnss_forecasting.funciones.tools import (
    dt_filename, 
    save_data_loss, 
    calculate_dtw, 
    epoch_time,
    get_memory)
from gnss_forecasting.dataload import load_dataset_iterators
from gnss_forecasting.train import train
from gnss_forecasting.evaluate import Evaluate



import httpx
import numpy as np
from scipy.spatial.distance import euclidean
from fastdtw import fastdtw
import psutil
from dataclasses import dataclass
from datetime import datetime
import pickle
import typer
import ujson as json

app = typer.Typer()

this_directory = Path(__file__).parent.resolve()

softDTW = DTW.create()

#Define a function to perform training
TOTAL = {
    Axis.E:0.,
    Axis.U:0.,
    Axis.N:0.
}

#Function to test neural network


def do_train(
        universal:UniversalManager, 
        iterator:UniversalIterator, 
        epochs:int=10, 
        machine:dict[str,Any]={}, 
        model_continue_train:bool=False, 
        model_name:str="",
        field_components:tuple[str,str]=(),
        now:str=""):

    # machine get id

    print("Try test do train")
    epoch_register = last_register(machine["id"])

    # machine load model
    if epoch_register and model_continue_train:
        pass

    # done

    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    cpu = 'cpu'

    best_valid_loss = float('inf')

    universal.train()
    total = {
        Axis.E:0.,
        Axis.U:0.,
        Axis.N:0.
    }

    loss_metrics_train = LossAndMetricsManager.create()
    loss_metrics_valid = LossAndMetricsManager.create()
    # select axes to train
    axes = [Axis.E,Axis.N,Axis.U]
    model_name ="LSTM"
    
    iterator.set_stage("validation")
    valid_iterator = iterator.get()
    evaluate = Evaluate(
         valid_iterator, 
         universal,
         device,
         loss_metrics_valid, 
         softDTW, 
         axes, 
         model_name)



    for epoch in range(epochs):
        loss_metrics_train.new_metrics(epoch)
        loss_metrics_valid.new_metrics(epoch)

        start_time = time.time()
        # train part
        iterator.set_stage("train")
        train_iterator = iterator.get()
        train(
            train_iterator, 
            universal, 
            device, 
            epoch,
            loss_metrics_train, 
            softDTW, 
            axes, 
            machine, 
            evaluate,
            model_name)


        end_time = time.time()

        for axis in axes:
            loss_metrics_train.set_status(axis)
            loss_metrics_valid.set_status(axis)
            train_loss, train_acc, train_dtw = loss_metrics_valid.elems()
            valid_loss, valid_acc, valid_dtw = loss_metrics_valid.elems()

            data_loss = {
                "train":{"loss":train_loss, "dtw":train_dtw},
                "valid":{"loss":valid_loss, "dtw":valid_dtw}
            }
            save_data_loss(model_name, epoch, data_loss, axis, now)
            # get the tail
            mean_valid_loss = np.mean([b for a,b in valid_loss[-600::]])
            mean_train_loss = np.mean([b for a,b in train_loss[-600::]])

            #If we find a smaller loss, we save the model
            if mean_valid_loss < best_valid_loss:
              best_valid_loss = mean_valid_loss

            model_path = universal.save(epoch, best_valid_loss, now, axis, machine)
            # measure time
            epoch_mins, epoch_secs = epoch_time(start_time, end_time)

            logging.info(f'Axis {axis},\t Epoch: {epoch+1:02} | Epoch Time: {epoch_mins}m {epoch_secs}s')
            logging.info(f'Axis {axis},\t Train Loss: {mean_train_loss:.3f}')
            logging.info(f'Axis {axis},\t Val. Loss: {mean_valid_loss:.3f}')
    return model_path

@app.command()
def run(
        dataset_selection,
        focus_stations:list[str],
        machine_id:Optional[int]=None,
        group_point:Optional[str]=None, 
        settings:dict[str, Any]={}
):

    now = dt_filename()
    lstm_layers = settings["lstm_layers"]
    #nn.CrossEntropyLoss()# clasification
    print(torch.cuda.is_available())

    # flag to determine if continue training
    model_continue_train = False


    # define by group the files of data
    # that are assigned to
    dataset_selection = {
        Stage.TRAIN: list(range(0,10)),
        Stage.VALIDATION: [10,11],
        Stage.TEST:[12]
    }
    mname = settings.get("name",0)
    model_name = f"rnn_lstm_{mname}"
    """
    Get log filename from machine already exists

    """
    filename = None
    if model_continue_train:
        machine_id=1
        machine = get_machine(machine_id)
        filename = machine.get("log_path")    

    log_filename = create_logging(
        this_directory.parent, 
        model_name,
        logging.DEBUG, 
        filename)


    # define hiperparameters
    learning_rate = settings.get("learning_rate",0.01)
    momentum = settings.get("momentum",0.01)
    #numbers valid on colab
    batch_size_train = settings.get("batch_size_train",1024)
    batch_size_test = settings.get("batch_size_test", 512)
    epochs = settings.get("epoch",1)
    window = settings.get("window",120)
    fact_future = settings.get("fact_future",0.5)
    future = int(window * fact_future)


    # create the manager for 3 axis
    models_path = this_directory.parent.parent.parent / f"models/{model_name}/"
    models_path.mkdir(parents=True, exist_ok=True)
    logging.info(f"Models path {models_path}")

    channels=1
    model_manager = ModelManager.create(
        RNNLSTM, 
        models_path,
        channels, 
        window=window, 
        future=future)
    optimizer = OptimizerManager.create(
        model_manager, 
        learning_rate)
    criterion = CriterionManager.create()

    datasets_path = this_directory.parent.parent.parent / "datasets"

    field_components = ("pure_backshifted","err")

    datasetpath, universal_iterator = load_dataset_iterators(
        datasets_path,
        dataset_selection, 
        batch_size_train,
        window, 
        future,
        group_point, 
        field_components,
        focus_stations)

    # move to cuda
    universal = UniversalManager(
        model_manager, 
        optimizer, 
        criterion)


    if machine_id:
        # if exist the checkpoint then load at model_ts timestamp the
        # model using universal load method
        universal.load_last(machine_id,optimizer)

    ### dataset iterators
    print(f"Group point to continue {group_point}")
    if not group_point:
        register_params = {
            "name":model_name,
            "description":"""This CNN LSTM model on cuda is created to
            explore the potencial of sequence networks""",
            "hyperparameters":{
                **model_manager.main_parameters(), 
                "learning_rate":learning_rate, 
                "selection":dataset_selection,
                "momentum":momentum,
                "lstm_layers":lstm_layers,
                "batch_size":{
                    "train":batch_size_train, 
                    "test":batch_size_test}},
            "optimizer":"OptimizerManager|Adam",
            "criterion":"CriterionManager|MSELoss",
            "epochs":epochs,
            "log_path":str(log_filename),
            "dataset_path":str(datasetpath.parent),
            "dataset_size":getFolderSize(str(datasetpath.parent)),
            "ml_path":str(models_path)
        }


        if not model_continue_train:
            machine = new_machine(register_params)
            name = register_params['name']
            mid  = machine['id']
            logging.info(f"Result register machine {name} :: {mid}")
        ###########
    else:
        machine = get_machine(machine_id)
    
    start = datetime.now()
    model_path = do_train(
        universal, 
        universal_iterator, 
        epochs, 
        machine, 
        model_continue_train, 
        model_name,
        field_components,
        now=now)
    end = datetime.now()
    print("Total duration train model",(end-start).total_seconds(),"Seconds")
    return model_path


if __name__=="__main__":
    app()
