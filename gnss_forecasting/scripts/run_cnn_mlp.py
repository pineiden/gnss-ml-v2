import typer
from cnn_mlp import run as cnn_mlp
from gnss_forecasting.managers import (
    Stage
)
from gnss_forecasting.funciones import read_file

app = typer.Typer()

@app.command()
def run(fsettings:Path):
    params = read_file(fsettings)
    settings = params["settings"]
    dselect = params["dataset_selection"]
    dataset_selection = {
        Stage.TRAIN: dselect["train"],
        Stage.TEST: dselect["test"],
        Stage.VALIDATION: dselect["validation"]
    }

    fstations = settings["stations"]
    focus_stations = [s.strip() for s in fstations.split(",")]

    cnn_mlp(
        dataset_selection, 
        focus_stations,
        machine_id=None,
        group_point=None,
        settings=settings
    )


if __name__=="__main__":
    app()
