import logging
import math
import time
import pickle
from pathlib import Path
from datetime import datetime
from typing import Dict,List,Any,Optional


# contrib
import numpy as np
from rich import print
import typer
import torch
import ujson as json

app = typer.Typer()

#loca
from gnss_forecasting.models import RNNLSTM
from gnss_forecasting.funciones import new_machine, last_register, get_machine
from gnss_forecasting.managers import (
    Stage,
    Axis,
    Switcher,
    ModelManager,
    OptimizerManager,
    CriterionManager,
    UniversalManager,
    LossAndMetrics,
    LossAndMetricsManager,
    UniversalIterator)
from gnss_forecasting.dataload import load_dataset_iterators
from gnss_forecasting.evaluate import Evaluate
from gnss_forecasting.funciones import DTW


this_directory = Path(__file__).parent.resolve()
datasets_path = this_directory.parent.parent.parent / "datasets"

# hiperparametros
def load_model(models_path:Path):
    learning_rate = 0.001
    momentum = 0.3
    #numbers valid on colab
    batch_size_train = 1024
    batch_size_test = 512
    epochs = 1
    window = 120
    future = int(window * .5)

    channels = 1

    model_manager = ModelManager.create(
        RNNLSTM, 
        models_path,
        channels, 
        window=window, 
        future=future)
    optimizer = OptimizerManager.create(
        model_manager, 
        learning_rate)


    epoch = 0
    for axe in Axis:
        model_manager.load(axe, optimizer, epoch)


    print(model_manager.E, model_manager.N, model_manager.U)
    return model_manager, optimizer


def load_datasets(
        model_manager, 
        optimizer, 
        field_components, 
        item, 
        code):
    # este controla el iterador sobre el grupo de estaciones
    focus_stations = code#"UTAR"]#, "CONS", "PTAR"]
    group_point = None
    learning_rate = 0.001
    momentum = 0.3
    #numbers valid on colab
    batch_size_train = 1024
    batch_size_test = 512
    epochs = 1
    window = 120
    future = int(window * .5)

    # seleccionar los tramos a evaluar
    dataset_selection = {
        Stage.TRAIN: [0],
        Stage.TEST: [0],
        Stage.VALIDATION:[0]
    }

    criterion = CriterionManager.create()
    # move to cuda
    universal = UniversalManager(
        model_manager, 
        optimizer, 
        criterion)

    datasets_path = this_directory.parent.parent.parent / "mini_datasets"
    datasetpath, universal_iterator = load_dataset_iterators(
        datasets_path,
        dataset_selection, 
        batch_size_train,
        window, 
        future,
        group_point, 
        field_components,
        focus_stations)

    return universal, universal_iterator



def do_valid(
        arch,
        code,
        universal,
        iterator,
        epochs,
        field_components, 
        item
):

    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    universal.eval()

    total = {
        Axis.E:0.,
        Axis.U:0.,
        Axis.N:0.
    }

    loss_metrics_train = LossAndMetricsManager.create()
    loss_metrics_valid = LossAndMetricsManager.create()
    # select axes to train
    axes = [Axis.E,Axis.N,Axis.U]
    iterator.set_stage("validation")
    valid_iterator = iterator.get()
    softDTW = DTW.create()
    model_name = f"{arch}_micro_{code}"

    #breakpoint()

    stations = [c for c in valid_iterator.dataset.focus_stations]

    #breakpoint()
    evaluate = Evaluate(
         valid_iterator, 
         universal,
         device,
         loss_metrics_valid, 
         softDTW, 
         axes, 
         model_name)

    loss_dtw = {"model_name":model_name}
    start = datetime.utcnow()

    collection = []
    for station in stations:
        evaluate.set_station(station)
        # here put the model check
        station_dataset, loss, dtw = evaluate.run_get_data()
        collection.append(station_dataset)
            # return results
        loss_dtw[station]={"loss":loss.dict(), "dtw":dtw.dict()}
    path = datasets_path / f"loss_dtw/{model_name}.json"
    end = datetime.utcnow()
    loss_dtw["duration"] = (end-start).total_seconds()
    loss_dtw["dt"] = end.isoformat()
    loss_dtw.update(item)
    txt = json.dumps(loss_dtw)
    path.write_text(txt)
    # guardar datos resultado
    return collection

def run_cobertura(
        arch:str, 
        code:list[str], 
        path:str, 
        item):
    # primero, obtenermos la ruta del modelo
    models_path = Path(path)
    model_manager, optimizer = load_model(models_path)
    print(model_manager)
    field_components = ("pure_backshifted","err")
    #breakpoint()

    universal, universal_iterator = load_datasets(
        model_manager,
        optimizer,
        field_components, 
        item, 
        code)
    print(universal, universal_iterator)
    epochs = 1
    collection = do_valid(
        arch,
        code,
        universal,
        universal_iterator,
        epochs,
        field_components,
        item
    )
    return collection
    
BASE = Path(__file__).parent.parent.parent
exp_path = BASE / "experimentos"
exp_path.mkdir(exist_ok=True)

@app.command()
def run(arch:str, stations:str, path:Optional[str]):
    if not path:
        path = "/content/drive/MyDrive/models/cnn_lineal_LSTM_MICRO_UTAR_2024_06_10_02_04_26_learningrate_0.1/machine_525/2024_06_10"
    if stations!="":
        stations = [s.strip() for s in stations.split(",")]
    else:
        stations = []
    print("Tun cobertura")
    collection = run_cobertura("baseline", stations, path, {})

    sts = "_".join(stations)
    if len(sts)>=50:
        sts = sts[0:50]
    archivo = f"rnnlstm_arch_{sts}.data"
    path = exp_path / archivo
    dbytes = pickle.dumps(collection)
    path.write_bytes(dbytes)
    print("Data=>",path)

if __name__=="__main__":
    app()
