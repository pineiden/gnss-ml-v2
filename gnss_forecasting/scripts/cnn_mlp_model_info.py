import logging
import math
import time
import pickle
from pathlib import Path
from datetime import datetime
from typing import Dict,List,Any,Optional


# contrib
import numpy as np
from rich import print
import typer
import torch
import ujson as json
from torchinfo import summary

app = typer.Typer()

#loca
from gnss_forecasting.models import CNNLinealBaseline
from gnss_forecasting.funciones import new_machine, last_register, get_machine
from gnss_forecasting.managers import (
    Stage,
    Axis,
    Switcher,
    ModelManager,
    OptimizerManager,
    CriterionManager,
    UniversalManager,
    LossAndMetrics,
    LossAndMetricsManager,
    UniversalIterator)
from gnss_forecasting.dataload import load_dataset_iterators
from gnss_forecasting.evaluate import Evaluate
from gnss_forecasting.funciones import DTW

def load_model(models_path:Path):
    learning_rate = 0.001
    momentum = 0.3
    #numbers valid on colab
    batch_size_train = 1024
    batch_size_test = 512
    epochs = 1
    window = 120
    future = int(window * .5)

    channels = 1

    model_manager = ModelManager.create(
        CNNLinealBaseline, 
        models_path,
        channels, 
        window=window, 
        future=future)
    optimizer = OptimizerManager.create(
        model_manager, 
        learning_rate)


    epoch = 0
    for axe in Axis:
        model_manager.load(axe, optimizer, epoch)


    print(model_manager.E, model_manager.N, model_manager.U)
    return model_manager, optimizer


@app.command()
def show_model_info(model_path:Path):
  name = "baseline_cnn_mlp_general"
  model_manager, optimizer = load_model(model_path)
  size = (1024, 1, 120, 10)
  print(size)
  info = dict(
    N=summary(model_manager.N,input_size=size), 
    E=summary(model_manager.E,input_size=size),
    U=summary(model_manager.U,input_size=size))
  path = Path(f"{name}.info")
  path.write_text(json.dumps(info))
  print(path)
  print(info)


if __name__=="__main__":
  app()