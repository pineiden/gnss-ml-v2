import typer
from gnss_forecasting.scripts.transformer import run as run_transformer
from gnss_forecasting.managers import (
    Stage
)
from pathlib import Path
from gnss_forecasting.funciones import read_file
from rich import print


app = typer.Typer()

@app.command()
def run(fsettings:Path):
    params = read_file(fsettings)
    print(params)
    settings = params["settings"]
    dselect = params["dataset_selection"]
    dataset_selection = {
        Stage.TRAIN: dselect["train"],
        Stage.TEST: dselect["test"],
        Stage.VALIDATION: dselect["validation"]
    }

    fstations = settings["stations"]
    focus_stations = []
    if fstations!="":
        focus_stations = [s.strip() for s in fstations.split(",")]

    run_transformer(
        dataset_selection, 
        focus_stations,
        machine_id=None,
        group_point=None,
        settings=settings
    )


if __name__=="__main__":
    app()
