import logging
import torch
from pathlib import Path
from datetime import datetime
import ujson as json
from typing import Dict,List,Any,Optional
import numpy as np
import ujson as json

from gnss_forecasting.managers import (
    Axis,
    UniversalManager,
    LossAndMetricsManager)
from gnss_forecasting.funciones.tools import (
    dt_filename, 
    dt_format,
    save_data_loss, 
    calculate_dtw, 
    epoch_time,
    get_memory)

class ArrayStats:
    values: Dict[str, Dict[str, np.array]]

    def __init__(self, name, main_code, values, *args, **kwwargs):
        self.name = name
        self.main_code = main_code
        self.values = values
    
    @property
    def mean(self):
        return {axis:{k:np.mean(v) for k,v in data.items()} for axis,data in self.values.items()}

    @property
    def std(self):
        return {axis:{k:np.std(v) for k,v in data.items()} for axis,data in self.values.items()}

    def dict(self):
        return {
            "name": self.name,
            "code": self.main_code,
            "mean": self.mean,
            "std": self.std,
        }

    def __repr__(self):
        return json.dumps(self.dict())



class Evaluate:

    def __init__(self,
            iterator, 
            universal:UniversalManager,
            device:str,
            loss_metrics:LossAndMetricsManager, 
            softDTW, 
            axes, 
            model_name:str="baseline"):
        self.iterator = iterator
        self.universal = universal
        self.device = device
        self.loss_metrics = loss_metrics
        self.softDTW = softDTW
        self.axes = axes
        self.model_name = model_name

    def set_station(self, station_code:str):
        self.main_code = station_code

    def run(self, epoch:int):

        iterator = self.iterator
        iterator.set_mode("eval")
        universal = self.universal
        device = self.device
        loss_metrics = self.loss_metrics
        softDTW = self.softDTW
        axes = self.axes
        model_name = self.model_name


        now = datetime.utcnow()
        dt = dt_format(now)
        path = Path(__file__).parent.parent /  f"samples/{model_name}/{dt}"
        path.mkdir(parents=True, exist_ok=True)

        print("Samples->", path)

        #We put the network in testing mode
        #In this mode, Pytorch doesn't use features only reserved for 
        #training (dropout for instance)    
        universal.eval()

        counter = 0
        save_step = 500
        future_counter = counter + save_step

        # set station
        print("Iterator", iterator, self.main_code)
        iterator.set_station(self.main_code)
        print("Evaluate the model -> {self.main_code}")

        acc_loss = {axis:{} for axis in axes}
        acc_dtw = {axis:{} for axis in axes}

        with torch.no_grad(): #disable the autograd engine (save
            #computation and memory)

            for (main_code, group, ts_fft), selector in iterator:
                loss_metrics.new_metrics(epoch)
                for axis in axes:

                    if main_code not in acc_loss[axis]:
                        acc_loss[axis][main_code] = []
                    if main_code not in acc_dtw[axis]:
                        acc_dtw[axis][main_code] = []


                    loss_metrics.set_status(axis)

                    selector.set_status(axis)

                    (x, x_err, y), distances, position, other_positions, checkpoint, encoder = selector.values()

                    #breakpoint()
                    counter += x.shape[0]
                    #y = y[1,:].squeeze(dim=2)
                    logging.info(f"Iteraring on axis {axis}")
                    universal.set_status(axis)
                    softDTW.set_status(axis)
                    model, optimizer, criterion = universal.objects()
                    model.to(device)
                    model.set_distances(distances)
                    model.set_position(position)
                    model.set_other_positions(other_positions)
                    model.set_encoder(encoder)

                    x = x.cuda()
                    x_err = x_err.cuda()
                    y = y.cuda()

                    if model_name.startswith("Transformer"):
                        y_pred, ts = model(x,y) #Feed the network with data
                    else:
                        y_pred, ts = model(x) #Feed the network with data

                    loss = criterion(y_pred, y)
                    # accuracy not in regression
                    dtw = calculate_dtw(softDTW,y_pred, y)

                    acc_loss[axis][main_code].append(loss.item())
                    acc_dtw[axis][main_code].append(dtw.item())
                    print("LOSS->", loss)
                    print("DTW->", dtw)
                    print("DIFF", loss-dtw)
                    print("Encoder", encoder)
                    print(f"Counter {counter}, future {future_counter}", counter%save_step>=0 and counter>future_counter)


                    if counter%save_step>=0 and counter>future_counter:

                        print("Counter samples->", counter)

                        last_one = -1
                        xg = x[:,:,:,5::]
                        #breakpoint()
                        x_dataset = {}
                        for i, g in enumerate(group):
                            x_dataset[g] = xg[last_one,:,:,i].tolist()
                        future_counter = counter + save_step
                        print(x_dataset.keys())

                        #breakpoint()

                        item = {
                            "ts":[v[last_one,:].tolist() for k,v in ts_fft.items()
                                  if k==axis.name.upper()],
                            "x":x_dataset,
                            "ts_dailysecs":list(map(int, ts[last_one,:])),
                            "y":y[last_one,:].tolist(),
                            "y_pred":y_pred[last_one,:].tolist(),
                            "loss":loss.item(),
                            "dtw":dtw.item(),
                            "axis":axis.name,
                            "station":main_code
                        }


                        data = json.dumps(item, indent=4)
                        sample_path = path / f"{main_code}_{epoch}_{counter}.json"
                        print(f"Saving sample to {sample_path}")
                        sample_path.write_text(data)
                    print("Loading to loss_metrics")
                    metric = loss_metrics.new_loss_metrics()
                    metric.dtw = dtw.item()
                    metric.loss = loss.item()


        print(f"Final counter {counter}")
        loss_stats  = ArrayStats("loss", self.main_code, acc_loss)
        dtw_stats  = ArrayStats("dtw", self.main_code, acc_dtw)
        return loss_stats, dtw_stats



    def run_get_data(self):
        epoch = 0
        iterator = self.iterator
        iterator.set_mode("eval")
        universal = self.universal
        device = self.device
        loss_metrics = self.loss_metrics
        softDTW = self.softDTW
        axes = self.axes
        model_name = self.model_name


        now = datetime.utcnow()
        dt = dt_format(now)
        path = Path(__file__).parent.parent /  f"samples/{model_name}/{dt}"
        path.mkdir(parents=True, exist_ok=True)

        print("Samples->", path)

        #We put the network in testing mode
        #In this mode, Pytorch doesn't use features only reserved for 
        #training (dropout for instance)    
        universal.eval()

        counter = 0
        save_step = 500
        future_counter = counter + save_step

        # set station
        print("Iterator", iterator, self.main_code)
        iterator.set_station(self.main_code)
        print("Evaluate the model -> {self.main_code}")

        acc_loss = {axis:{} for axis in axes}
        acc_dtw = {axis:{} for axis in axes}

        with torch.no_grad(): #disable the autograd engine (save
            #computation and memory)
            dataset = {"counter":0}
            for (main_code, group, ts_fft), selector in iterator:
                codes = tuple(group)
                if main_code not in dataset:
                    dataset[main_code] ={}
                if codes not in dataset[main_code]:
                    dataset[main_code][codes] = {"E":[],"N":[],"U":[]}
                loss_metrics.new_metrics(epoch)
                for axis in axes:
                    dataset["counter"] += 1

                    if main_code not in acc_loss[axis]:
                        acc_loss[axis][main_code] = []
                    if main_code not in acc_dtw[axis]:
                        acc_dtw[axis][main_code] = []


                    loss_metrics.set_status(axis)

                    selector.set_status(axis)

                    (x, x_err, y), distances, position, other_positions, checkpoint, encoder = selector.values()

                    #breakpoint()
                    counter += x.shape[0]
                    #y = y[1,:].squeeze(dim=2)
                    logging.info(f"Iteraring on axis {axis}")
                    universal.set_status(axis)
                    softDTW.set_status(axis)
                    model, optimizer, criterion = universal.objects()
                    model.to(device)
                    model.set_distances(distances)
                    model.set_position(position)
                    model.set_other_positions(other_positions)
                    model.set_encoder(encoder)

                    xc = x.to("cpu")
                    yc = y.to("cpu")

                    x = x.cuda()
                    x_err = x_err.cuda()
                    y = y.cuda()


                    if model_name=="Transformer":
                        y_pred, ts = model(x,y) #Feed the network with data
                    else:
                        y_pred, ts = model(x) #Feed the network with
                        #data

                    # prepare item for dataset
                    item = {
                        "_in": {"ts":ts.to("cpu"), 
                                "x":xc,
                                "y": yc},
                        "_out": y_pred.to("cpu")
                    }

                    dataset[main_code][codes][axis.name].append(item)

                    loss = criterion(y_pred, y)
                    # accuracy not in regression
                    dtw = calculate_dtw(softDTW,y_pred, y)

                    acc_loss[axis][main_code].append(loss.item())
                    acc_dtw[axis][main_code].append(dtw.item())
                    print("LOSS->", loss)
                    print("DTW->", dtw)
                    print("DIFF", loss-dtw)
                    print("Encoder", encoder)
                    print(f"Counter {counter}, future {future_counter}", counter%save_step>=0 and counter>future_counter)


                    if counter%save_step>=0 and counter>future_counter:

                        print("Counter samples->", counter)

                        last_one = -1
                        xg = x[:,:,:,5::]
                        #breakpoint()
                        x_dataset = {}
                        for i, g in enumerate(group):
                            x_dataset[g] = xg[last_one,:,:,i].tolist()
                        future_counter = counter + save_step
                        print(x_dataset.keys())

                        #breakpoint()

                        item = {
                            "ts":[v[last_one,:].tolist() for k,v in ts_fft.items()
                                  if k==axis.name.upper()],
                            "x":x_dataset,
                            "ts_dailysecs":list(map(int, ts[last_one,:])),
                            "y":y[last_one,:].tolist(),
                            "y_pred":y_pred[last_one,:].tolist(),
                            "loss":loss.item(),
                            "dtw":dtw.item(),
                            "axis":axis.name,
                            "station":main_code
                        }


                        data = json.dumps(item, indent=4)
                        sample_path = path / f"{main_code}_{epoch}_{counter}.json"
                        print(f"Saving sample to {sample_path}")
                        sample_path.write_text(data)
                    print("Loading to loss_metrics")
                    metric = loss_metrics.new_loss_metrics()
                    metric.dtw = dtw.item()
                    metric.loss = loss.item()


        print(f"Final counter {counter}")
        loss_stats  = ArrayStats("loss", self.main_code, acc_loss)
        dtw_stats  = ArrayStats("dtw", self.main_code, acc_dtw)
        return dataset, loss_stats, dtw_stats
