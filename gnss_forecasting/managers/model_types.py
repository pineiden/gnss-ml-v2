from typing import TypeVar
from enum import IntEnum

Model = TypeVar('Model')
GNSSObject = TypeVar('GNSSObject')

class Axis(IntEnum):
    N=0
    E=1
    U=2

    

Iterator = TypeVar("Iterator")
