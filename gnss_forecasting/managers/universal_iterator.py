from dataclasses import dataclass
from .stage import Stage
from .model_types import Iterator


@dataclass
class UniversalIterator:
    train: Iterator
    validation: Iterator
    test: Iterator
    stage:Stage = Stage.TRAIN

    def set_stage(self, name):
        name = name.lower()
        match name:
            case "train":
                self.stage = Stage.TRAIN
            case "validation":
                self.stage = Stage.VALIDATION
            case "test":
                self.stage = Stage.TEST
            case _:
                self.stage = Stage.TRAIN


    def get(self):
        
        match self.stage:
            case Stage.TRAIN:
                return self.train
            case Stage.VALIDATION:
                return self.validation
            case Stage.TEST:
                return self.test
