from dataclasses import dataclass
from typing import TypeVar, Any
from pathlib import Path
from torch import nn
from .model_types import Axis, Model
from .switcher import Switcher
import torch
import logging
from datetime import datetime

DT_FORMAT ="%Y_%m_%d"

def dt_format(dt:datetime)->str:
    return dt.strftime(DT_FORMAT)

def str_format_dt(dt:str)->datetime:
    return datetime.strptime(dt, DT_FORMAT)


@dataclass
class ModelManager(Switcher):
    path: Path

    def train(self):
        self.N.train()
        self.E.train()
        self.U.train()


    def save(self, 
             axis:Axis, 
             optimizer, 
             epoch:int,  
             loss:float,
             now:str, 
             machine:dict[str, Any]):
        data = {
            "epoch":epoch,
            "optimizer_state_dict": optimizer.state_dict(),
            "loss":loss,
            "now": now,
            "machine":machine
        }
        mid = machine["id"]
        path = self.path / f"machine_{mid}" / now
        path.mkdir(exist_ok=True, parents=True)
        match axis:
            case Axis.N:
                data["model_state_dict"] = self.N.state_dict()
                torch.save(data, path / f"N_{epoch}.pt")
            case Axis.U:
                data["model_state_dict"] = self.U.state_dict()
                torch.save(data, path  /f"U_{epoch}.pt")
            case Axis.E:
                data["model_state_dict"] = self.E.state_dict()
                torch.save(data, path  /f"E_{epoch}.pt")
        return path


    def load(self, axis:Axis, optimizer, epoch):
        path = self.path
        match axis:
            case Axis.N:
                checkpoint = torch.load(path /f"N_{epoch}.pt")
                self.N.load_state_dict(checkpoint["model_state_dict"])
                optimizer.N.load_state_dict(checkpoint["optimizer_state_dict"])
                logging.info(f"Loaded correctly model on axis {axis}")
                return checkpoint["epoch"], checkpoint["loss"]
            case Axis.U:
                checkpoint = torch.load(path /f"U_{epoch}.pt")
                self.U.load_state_dict(checkpoint["model_state_dict"])
                optimizer.U.load_state_dict(checkpoint["optimizer_state_dict"])
                logging.info(f"Loaded correctly model on axis {axis}")
                return checkpoint["epoch"], checkpoint["loss"]
            case Axis.E:
                checkpoint = torch.load(path /f"E_{epoch}.pt")
                self.E.load_state_dict(checkpoint["model_state_dict"])
                optimizer.E.load_state_dict(checkpoint["optimizer_state_dict"])
                logging.info(f"Loaded correctly model on axis {axis}")
                return checkpoint["epoch"], checkpoint["loss"]
                
    def eval(self):
        self.E.eval()
        self.N.eval()
        self.U.eval()

    def train(self):
        self.E.train()
        self.N.train()
        self.U.train()

    def to(self,device):
        self.E.to(device)
        self.N.to(device)
        self.U.to(device)

    @classmethod
    def create(cls, class_object, path, *args, **kwargs):
        if "settings" in kwargs:
            settings = kwargs.pop("settings")
            for k in settings:
                if k in kwargs:
                    kwargs.pop(k)
            E = class_object(**kwargs, **settings)
            N = class_object(**kwargs, **settings)
            U = class_object(**kwargs, **settings)
        else:
            E = class_object(*args, **kwargs)
            N = class_object(*args, **kwargs)
            U = class_object(*args, **kwargs)
        return ModelManager(
            N=N, 
            E=E, 
            U=U, 
            status=Axis.E, 
            path=path)


    def main_parameters(self):
        return {
            "e":self.E.main_parameters(), 
            "n":self.N.main_parameters(), 
            "u":self.U.main_parameters()}
