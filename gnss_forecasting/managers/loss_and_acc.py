from dataclasses import dataclass, asdict
from typing import Dict
import ujson as json
from .model_types import Axis
from .switcher import Switcher

@dataclass
class LossAndMetrics:
    """
    Metricas consideridadas
    """
    epoch: int
    loss: float
    acc: float
    dtw:float

    @classmethod
    def create(cls, epoch)->'LossAndMetrics':
        return LossAndMetrics(epoch=epoch, loss=0.0,acc=0.0,dtw=0.0)

    def dict(self):
        return self.__dict__

@dataclass
class LossAndMetricsManager(Switcher):
    memory:int=1000

    @classmethod
    def create(cls)->'LossAndMetricsManager':
        total = {
            Axis.E:0.0,
            Axis.N:0.0,
            Axis.U:0.0
        }
        return LossAndMetricsManager(
            E=[],
            N=[],
            U=[],
            status=Axis.E
        )

    @property
    def total(self):
        return len(self.E)

    def set_memory(self, mem:int):
        self.memory = mem
        while len(self.N)>self.memory:
            self.N.pop(0)
        while len(self.E)>self.memory:
            self.E.pop(0)
        while len(self.U)>self.memory:
            self.U.pop(0)

    def set_epoch(self, epoch:int):
        self.epoch = epoch

    def new_metrics(self, epoch):
        self.set_epoch(epoch)


    def new_loss_metrics(self):
        match self.status:
            case Axis.N:
                instance = self.N
            case Axis.U:
                instance = self.U
            case Axis.E:
                instance = self.E
        metric = LossAndMetrics.create(self.epoch)
        if len(instance)>self.memory:
            instance.pop(0)
        metric = LossAndMetrics.create(self.epoch)
        instance.append(metric)
        return metric

    def add_loss(self, val:float):
        match self.status:
            case Axis.N:
                instance = self.N
            case Axis.U:
                instance = self.U
            case Axis.E:
                instance = self.E

    def add_acc(self, val:float):
        match self.status:
            case Axis.N:
                instance = self.N
            case Axis.U:
                instance = self.U
            case Axis.E:
                instance = self.E


    def add_dtw(self, val:float):
        match self.status:
            case Axis.N:
                instance = self.N
            case Axis.U:
                instance = self.U
            case Axis.E:
                instance = self.E
        if len(instance)>self.memory:
            instance.pop(0)

    def loss(self)->float:
        match self.status:
            case Axis.N:
                return [(m.epoch, m.loss) for m in self.N]
            case Axis.U:
                return [(m.epoch, m.loss) for m in self.U]
            case Axis.E:
                return [(m.epoch, m.loss) for m in self.E]


    def acc(self)->float:
        match self.status:
            case Axis.N:
                return [(m.epoch, m.acc) for m in self.N]
            case Axis.U:
                return [(m.epoch, m.acc) for m in self.U]
            case Axis.E:
                return [(m.epoch, m.acc) for m in self.E]



    def dtw(self)->float:
        match self.status:
            case Axis.N:
                return [(m.epoch, m.dtw) for m in self.N]
            case Axis.U:
                return [(m.epoch, m.dtw) for m in self.U]
            case Axis.E:
                return [(m.epoch, m.dtw) for m in self.E]



    def elems(self)->(float,float):
        return self.loss(), self.acc(), self.dtw()
            

    def dict_metrics(self):
        return json.dumps({
            "e":[m.dict() for m in self.E],
            "n":[m.dict() for m in self.N],
            "u":[m.dict() for m in self.U]})

