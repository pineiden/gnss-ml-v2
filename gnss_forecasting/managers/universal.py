from dataclasses import dataclass
import logging
from datetime import datetime

from .model import ModelManager
from .optimizer import OptimizerManager
from .criterion import CriterionManager
from .model_types import Axis
import re

DT_FORMAT ="%Y_%m_%d"

def dt_format(dt:datetime)->str:
    return dt.strftime(DT_FORMAT)

def str_format_dt(dt:str)->datetime:
    return datetime.strptime(dt, DT_FORMAT)

def get_number(value:str)->int:
    regex = re.compile("^\d+$")
    if regex.match(value):
        return int(value)

@dataclass
class UniversalManager:
    model: ModelManager
    optimizer: OptimizerManager
    criterion: CriterionManager

    def set_status(self, x):
        self.model.set_status(x)
        self.optimizer.set_status(x)
        self.criterion.set_status(x)

    def objects(self):
        return (
            self.model.get(), 
            self.optimizer.get(),
            self.criterion.get()
        )


    def save(self, 
        epoch:int, 
        loss:float, 
        now:str|datetime, 
        axis:Axis, 
        machine):
        self.set_status(axis)
        optimizer = self.optimizer.get()
        if isinstance(now, datetime):
          now = dt_format(now)
        path = self.model.save(axis, optimizer, epoch, loss, now, machine)
        logging.info(f"""Model saved on epoch {epoch}, loss {loss}, dt
        {now}, axis {axis}, machine_id {machine["id"]}""")    
        return path

    def load(self, optimizer, epoch, now:str):
        for axis in Axis:
            self.model.load(axis, optimizer, epoch, now)
            logging.info(f"""Model loaded on epoch {epoch}, dt {now}, axis {axis}, machine_id {machine["id"]}""")    
        

    def load_last(self, machine_id, optimizer):
        base_path = self.model.path / f"machine_{machine_id}"

        # last datetime for this machine_id
        dataset = []
        for p in base_path.glob("*"):
            dataset.append(str_format_dt(p.name))
        max_dt = dt_format(max(dataset))

        # last epoch for this machine_id on the max_dt
        epochs = []
        base_path_dt = base_path / max_dt
        for pname in base_path_dt.glob("*.pt"):
            if "_" in pname.stem:
                axis, epoch = pname.stem.split("_")
                number = get_number(epoch)
                if number:
                    epoch.append(number)

        # better->group by axis then max by axis
        max_epoch = max(epochs)
        self.load(optimizer, max_epoch, max_dt)
       

    def eval(self):
        self.model.eval()

    def train(self):
        self.model.train()
