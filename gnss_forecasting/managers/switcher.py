from .model_types import Axis, GNSSObject
from dataclasses import dataclass


@dataclass
class Switcher:
    N:GNSSObject
    E:GNSSObject
    U:GNSSObject
    status: Axis 

    def set_status(self,axis:str|Axis):
        if type(axis)==str:
            axis=axis.lower()

        match axis:
            case v if v in {"e", "east", "este", Axis.E}:
                self.status = Axis.E
            case v if v in {"u","up","arriba", Axis.U}:
                self.status = Axis.U
            case v if v in {"n", "north", "norte", Axis.N}:
                self.status = Axis.N
            case other:
                self.status = Axis.E


    def get(self)->GNSSObject:
        match self.status:
            case Axis.N:
                return self.N
            case Axis.U:
                return self.U
            case Axis.E:
                return self.E

