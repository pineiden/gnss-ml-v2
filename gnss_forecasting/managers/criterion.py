from dataclasses import dataclass
from torch import nn
from .model_types import Axis
from .switcher import Switcher



@dataclass
class CriterionManager(Switcher):

    @classmethod
    def create(cls):
        N = nn.MSELoss()
        E = nn.MSELoss()
        U = nn.MSELoss()
        return CriterionManager(N,E,U, status=Axis.E)

