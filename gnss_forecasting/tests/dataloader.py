import numpy as np
from pathlib import Path
from rich import print
import torch
import logging
from datetime import datetime
from dataloaders import GNSSDataloader
from datasets import GNSSDataset
from filelog import create_logging
import pickle
import json
from gnssml.managers import (
    Stage,Axis)
import sys

def read_path(path:Path):
    databytes = path.read_bytes()
    dataset = pickle.loads(databytes)
    return dataset

def read_json(path:Path):
    databytes = path.read_text()
    dataset = json.loads(databytes)
    return dataset

def get_positions(metadata):
    item = metadata["position"]["llh"]
    return [item["lon"], item["lat"], item["z"]]


if __name__=="__main__":
    group_point = None
    if len(sys.argv)>2:
        group_point = sys.argv[1]
        model_ts = sys.argv[2]

    print("Group Point", group_point)

    base = Path(__file__).parent.resolve().parent
    model_name = "test_gnssdataset"
    filename = "test_dataloger.log"
    create_logging(base, model_name, logging.DEBUG, filename)
    base = Path(__file__).parent.resolve()
    dataset = base.parent.parent / "datasets/timeserie"
    dataset_fft = base.parent.parent / "datasets/fft_out"
    groups = base.parent.parent / "datasets/graph_neightboor_stations_set.data"

    pairs_distance = read_path(base.parent.parent / "datasets/pairs_distance.data")
    positions = {item["code"]:get_positions(item) for item in
                 read_json(base.parent.parent/"datasets/metadata.json")}

    dataset_selection = {
        Stage.TRAIN: [0],
        Stage.VALIDATION: [1],
        Stage.TEST:[2]
    }


    dataset = GNSSDataset(
        dataset, 
        dataset_fft, 
        groups,
        pairs_distance,
        dataset_selection,
        positions,
        stage="train",
        window=100,
        test=True, 
        group_point=group_point)

    print(dataset.groups)
    print(dataset.indexes)
    print(dataset.re_groups)

    print("start iteration ::::")
    dataloader = GNSSDataloader(
        dataset, 
        batch_size=64)

    for i, selector in enumerate(dataloader):
        print(f"{i} -> ::")
        print(selector)
        print(selector.values())
        if i == 10:
            break
